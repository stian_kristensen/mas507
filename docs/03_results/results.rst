.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Results
***************

The strawberry field used for demonstration of the robot was set up specifications, described in :numref:`Section %s <Strawberry Field>`. Due the limited availability of strawberry plants the plants had to be moved during the test of the robot. Each plant location was marked with a cross in advance, as shown in :numref:`Figure %s <StrawberryField>`. The strawberry robot was set to start at the marked location, pick red strawberries detected while navigating towards the first aruco marker. Once the robot is a distance of 25cm from the aruco marker it turns 90 degrees to the right before turning right 25cm from the second aruco marker. The robot then picks any ripe berries detected while making it towards the third aruco marker at the finish line.

Three tests was conducted to ensure validity of the results from the test. The average time the robot used from picking the first strawberry to reaching the finish line was 2 minutes and 38 seconds, which are within the given 3 minutes limit for this task. The videos from each tests are appended in :numref:`Appendix %s <Appendix B>`


.. csv-table:: Results
  :name: resultTable
  :header: Run #, Ripe Berries Picked, Green berries picked, Time, Less than 3 minutes
  :widths: 25 25 25 25 25

    1st, 8/8, 0/2, 02:31 min, Yes
    2nd, 8/8, 0/2, 02:33 min, Yes
    3rd, 8/8, 0/2, 02:49 min, Yes
    Average:, 8/8, 0/2, 02:38 min, Yes


.. figure:: top_track_view.png
    :name: StrawberryField
    :scale: 30%
    :align: center
    :alt: field

    Implemented strawberry field.


.. 
    Thing we will be graded for:
    x Overall impression of the work
    X Execution, including concept development
    X Result (prototype)