.. MAS507 documentation master file

.. # define a hard line break for HTML
.. |br| raw:: html

   <br />


.. figure:: 00_globalFiles/uia.png
    :scale: 60%
    :align: center
    :alt: UiA Logo   

.. rst-class:: center-bold

   University of Agder

.. rst-class:: center

   Department of Engineering Sciences |br| Grimstad, Norway

.. figure:: 00_globalFiles/frontpage.jpg
    :scale: 60%
    :align: center
    :alt: UiA Logo   

MAS507 - Product Development and Project Management
===================================================

This website is created with the intention of documenting and presenting the work on the project in MAS506 - Product Development and Project Management in the Mechatronics master's programme. The work done is that of the development of a autonomous strawberry robot based on Jetson Nano. The development of the robot is done by analyzing concepts, locally producing parts and testing the robot in a controlled environment.

Student Group
--------------------------------

   * Ole Henrik Haakstad

            https://www.linkedin.com/in/oleh-haakstad
   
   * Stian André Kristensen

            https://www.linkedin.com/in/s-kristensen

   * Håkon Almelid Tvilde

            https://www.linkedin.com/in/htvilde

   * Tom Erik Vange

            https://www.linkedin.com/in/te-vange
            

Lecturers
--------------------------------
   * Sondre Sanden Tørdal, PhD

            Postdoctoral Fellow in Mehcatronics |br|
            https://www.uia.no/kk/profil/sondrest

   * Mette Mo Jakobsen, PhD

            Professor in Product Development and Project-based learning |br|
            https://www.uia.no/kk/profil/mettemj


.. toctree::
   :maxdepth: 3
   :caption: Project Content:
   :numbered:

   01_theory/theory
   02_method/method
   02_RobotSoftware/RobotSoftware
   03_results/results
   04_discussion/discussion
   05_conclusion/conclusion
   06_Bibliography/sources
   99_Appendices/00_appendix