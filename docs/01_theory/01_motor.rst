Motor 
=================================
This section describes general information about motor control

Servo Motor
-------------------------------------
The main purpose of a servo motor is to achieve controlled movement of a load.
Servo motors with limited rotation will have their rotation limited by the manufacturer and such

A servo motor typically features an encoder which is used for feedback of the motor shaft angle, gearing to increase the output torque, and controlled with a pulse width modulated (PWM) signal.
The motor shaft is controlled by a duty cycle in a PWM Period. Figure (REF) shows an example of this where an increase in the duty cycle increases the angle of the motor, as shown in Figure (REF).


Communication with Servo Motor
-------------------------------------
