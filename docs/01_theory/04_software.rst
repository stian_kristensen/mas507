Open Source Computer Vision Library
=============================================

OpenCV is an open-source real-time computer vision library, which provide free computer vision and machine learning application. OpenCV is wildly used, from large companies like Google, Microsoft, Intel and IBM to startups and students :cite:`openCV_about`. This project will use OpenCV to identify a strawberry and find its position referred to the robot’s camera. 


Color Recognition
=================================

The method of color recognition is to look at the image data and identify the colors present in the image. Different colors are defined with different values and can be compered to a particular color space. Unwanted colors can be removed, or masked, to make a binary image. This is necessary to find wanted color image points by edge detection. Edge detection in this project this is aided by using OpenCV for Python to make the task reachable. 


PnP algorithm  
===================================

.. figure:: Figures/PnP.png
    :name: pnpImage
    :scale: 80%
    :align: left
    :alt: nothing

    Coordinate transformation :cite:`pnp_image`.

Perspective-n-Point algorithm is estimating the pose of either the camera or the object of given 3D object points and their corresponding 2D image points.  Camera calibration is necessary for estimating the parameters of the camera. The Calibrated Camera parameters is used by the PnP algorithm, together with image points to estimate the position of the strawberry. The Camera calibration process was done by following the lectures notes from MAS507 – product development :cite:`UiAsubject`. 

