Jetbot
=================================

.. figure:: Figures/Jetbot.PNG
    :name: JetbotComponents
    :scale: 70%
    :align: right
    :alt: Jetbot   

    Jetbot Base :cite:`UiAsubject`.

The jetbots main component in the Jetson Nano, a small computer that can be used for image classification, object detection and more :cite:`jatsonBot`. The jetbot, Figure :numref:`Figure %s <JetbotComponents>` uses a setup of two independently driven wheels in the front with a unpowered caster wheel in the rear to manuever. A camera is also mounted on the jetbot, a single Li-Po battery is used to power the bot with all its components.