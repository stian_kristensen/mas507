.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Product Development
============================
Product development refers to all stages involved in bringing a product from concept or idea into a finalized product launched into a 
market. 
An example of product development phase and its components is shows in :numref:`Figure %s <conceptphase>`.

.. figure:: Figures/conceptPhase.png
    :name: conceptphase
    :scale: 60%
    :align: center
    :alt: image 1   

    Typical Product Development Phase :cite:`prodDesignDev`

Product Specifications
---------------------------------
Product specifications are the specifications given from the customer to the designers and engineers. 
These specifications usually requires translation from the "language of the customer" into technical 
requirements to be used by the designers and engineers :cite:`prodDesignDev`. The translation is required as a clear
customer interests provide no specifications into the design and construction of a product.

The process used to establish product specifications includes these five steps :cite:`prodDesignDev`:

    1. Gather raw data from customers.
    #. Interpret the raw data in terms of customer needs.
    #. Organize the needs into a hierarchy.
    #. Establish the relative importance of the needs.
    #. Reflect on the results and the process.

Target Specification
----------------------------------
Target specifications are specifications that are established before product concept, but after customer 
needs are identified. The preliminary specifications could be subjected to changes during 
the project and are contingent on the details of the chosen product concept :cite:`prodDesignDev`. 
The process of establishing target specifications are, in four steps :cite:`prodDesignDev`:

    1. Preparing list of metrics, which should be complete, dependent, variable and practical.
    #. Collect bench-marking information, to determine the relationship between the new product and competitive products.
    #. Set acceptable target values (Ideal and marginally acceptable), to guide subsequent stages of concept generation and selection, as well as refining specifications after the selection of product concept.
    #. Reflect on results then process, to ensure that the results are consistent with the project goals.


Final Specification
-----------------------------------
Target specifications are the specifications that are refined in the finalizing stages of a concept
and prepares for subsequent design and development. Broad and estimated specifications from the target 
stage are refined and made more precise :cite:`prodDesignDev`. The process of establishing final specifications can be 
described in the following five steps :cite:`prodDesignDev`:

    1. Developing a technical model for the product, for predicting the values of metrics.
    #. developing cost model of the product, to make sure that it is produced at the target cost.
    #. Refining specifications.
    #. Flowing down specifications for the product as a whole; from overall specification, to specifications for each subsystem.
    #. Reflecting on the process results.


Concept Generation
---------------------------------------------
Concept generated through a 5 step process :cite:`prodDesignDev`.

**1. Clarify**

A general understand of the problem is required in order to clarify the problem. Problems should be broken
down into multiple subproblem if necessary.

**2. Search Externally**

External search is finding existing solutions and should be performed continually throughout the development
process.

**3. Search Internally**

Internal search is the teams knowledge and creativity to create a concept. Brainstorming is a commonly used method for
stimulating a group of peoples ability to generate loads of ideas in a short amount of time :cite:`metteJacobBok`.

The book "*Produktutvikling : verktøykasse for utvikling av konkurransedyktige produkter*" by "*Mette Mo Jacobsen*" proposes
the following structured approach :cite:`metteJacobBok`:

    1. Formulate the issue in a clear and concise way.
    #. Create a group of 3-7 persons. The group should be interdisciplinary with multiple expertise. Avoid group hierarchy. 
    #. Pick a leader which ensures that the rules for brainstorming are upheld and can keep the brainstorming session going.
    #. Don't evaluate any of the suggestions, both negative and positive, during the brainstorming phase.
    #. Note all ideas either manually, as sketches or as recordings.
    #. End the brainstorming after 30-45 minutes.
    #. Evaluate the proposed ideas. A concretization and systematization of the ideas is recommended. New and more complex ideas may appear during a evaluation process.


**4. Explore Systematically**

From the external and internal searching there will be many solutions to the subproblems or problems. Systematic exploration of possibilities lets the team systematically explore all solutions to all problems with out 
the need to apply every solution to every problem. Two tools for managing this complexity are the "concept classification tree" and the "concept combination table". 


**5. Reflect on the solution and the process**

Reflection should be applied throughout the whole concept generation process. Examples of questions to ask:

    * Is the team developing confidence that the solution space had been fully explored? 
    * Are there alternative function diagrams?
    * Are there alternatives ways to decompose the problem?
    * Have external sources been thoroughly pursued?
    * Have ideas from everyone been accepted and integrated in the process?

Opportunities for improvements in subsequent iterations or future projects should be identified. 

Concept Evaluation
---------------------------------------------
The purpose of concept evaluation is to find the best overall solution. Solutions that are 
valuable in one area might perform poorly in other areas. A systematic evaluation of a concept 
is necessary to ensure that all criteria are considered and the results is the best result overall :cite:`metteJacobBok` .

A proposed 4 step approach to concept evaluation by "*Mette Mo Jakobsen*" :cite:`metteJacobBok`:

    1. Set up the evaluation criteria. These can originate from product specification and customer needs.
    #. Weigh the different criteria for evaluation if they have different significance for the outcome.
    #. The evaluation criteria can sometimes be subdivided into two groups; criteria related to user benefit and criteria related to cost.   
    #. Visualize the concept evaluation, an example is shown in :numref:`Table %s <conceptEvaluationTable>`. Example of cost evaluation is shown in :numref:`Table %s <costEvaluationTable>`.
    
.. csv-table:: Example of Evaluation with focus on the user :cite:`metteJacobBok`
  :name: conceptEvaluationTable
  :header: Criteria, Weight, Concept 1, Concept 2, Concept 3, Ideal
  :widths: 25 25 25 25 25 25

  *Function Dimension* |br| |br| High Capacity |br| Reliability, |br| |br| 0.2 |br| 0.4, |br| |br| 3 (=> 0.6) |br| 3 (=> 1.6), |br| |br| 3 (=> 0.6) |br| 4 (=> 1.2), |br| |br| 4 (=> 0.8) |br| 4 (=> 1.6), |br| |br| 4 (=> 0.8) |br| 4 (=> 1.6)
  *Market Dimension*
  *Production Dimension*
  *Design Dimension*
  *Use Dimension* |br| |br| Easy of Use, |br| |br| 0.3, |br| |br| 2 (=> 0.6), |br| |br| 3 (=> 0.9),|br| |br| 4 (=> 1.2), |br| |br| 4 (=> 1.2)
  Safety Dimension
  *Product life Dimension* |br| |br| Doesn't require much space, |br| |br| 0.1, |br| |br| 1 (=>0.1), |br| |br| 1 (=>0.1), |br| |br| 4 (=>0.4), 4 (=> 0.4)
  Sum |br| Relative Value, 1.0 |br| -, 2.6 |br| 0.65, 3.2 |br| 0.8, 4.0 |br| 1.0, 4.0 |br| 1.0


.. csv-table:: Example of Evaluation with focus on cost :cite:`metteJacobBok`
  :name: costEvaluationTable
  :header: Criteria, Weight, Concept 1, Concept 2, Concept 3, Ideal
  :widths: 25 25 25 25 25 25

  Product Dimension |br| |br| Number of Parts |br| Production Difficulty |br| Material Consumption, , |br| |br| 3 |br| 4 |br| 4, |br| |br| 3 |br| 2 |br| 4, |br| |br| 4 |br| 4 |br| 4, |br| |br| 4 |br| 4 |br| 4
  Sum |br| Relative Value, , 11.0 |br| 0.92, 9.0 |br| 0.75, 12.0 |br| 1.0, 12.0 |br| 1.0


Sustainable Development Goals 
--------------------------------------------
The sustainable development goals was introduced in 2012, with the objective to produce a set of universal goals that meets the environmental, political and economic challenges in the world. The goals replaced the Millennium Development Goals introduced in 2000 and focused on the indignity of poverty :cite:`un_background_goals`.

As of 2020 there are 17 goals to better the life conditions and the environment locally and globally, these sustainable development goals are the blueprint to achieve a better and more sustainable future for all. The primary goal is to prevent poverty, hunger, better health and well-being, quality education and more :cite:`Un_Goals`, with all the 17 goals shown in :numref:`Figure %s <UNgoals>`.

.. figure:: Figures/UN_goals.PNG
    :name: UNgoals
    :scale: 80%
    :align: center
    :alt: UN goals   

    United Nations Goals :cite:`Un_Goals`


Smarter and Better Teamwork
===============================

Smarter Teams
--------------------------------------
Roger Schwartz describes how to create a smarter team with the use of "*Eight Behaviors* " in his paper named "*Eight Behaviors for Smarter Teams*" :cite:`SmarterTeams`. 

The paper describes specific behavior that leads to improved team dynamic and how members work together. The "Eight Behaviors"
are created with the intention of providing clearer guidelines rather than "abstract notions".
The guidelines are also constructed such that they provide mindsets rather than procedures that are to be implemented.

The behaviors in "Eight Behaviors" are as follows :cite:`SmarterTeams`:

    1. State views and ask genuine questions.
    #. Share all relevant information.
    #. User specific examples and agree on what important words means.
    #. Explain reasoning and intention.
    #. Focus on interests, not positions.
    #. Test assumptions and inferences.
    #. Jointly design next steps.
    #. Discuss undiscussable issues.

.. 
    http://www.schwarzassociates.com/resources/articles/page/3/::


Assumption of the Mutual Learning Mindset.
----------------------------------------------

The assumptions of mutual learning mindsets are :cite:`SmarterTeams`:

    * I have some information; as do other people.
    * Each of us may see things that others don't.
    * I may be contributing to the problem.
    * Differences are opportunities for learning.
    * People may disagree with me and have pure motives.

The core values of mutual learning mindsets are :cite:`SmarterTeams`:

    * Transparency
    * Curiosity
    * Informed Choice
    * Accountability
    * Compassion 

Operating from the "Mutual Learning" assumptions and core values are something that many of us operate automatically and with relative ease when working with 
people that share our view or that we have a strong working relationship with. However these core values and assumptions are most important when 
we work with people that hold different views from our own and in teams where a good working relationship exists. As our behavior are 
influenced by our core values and assumptions practicing "Mutual Learning Mindsets" is important to improve team dynamic :cite:`SmarterTeams`. 


Team Contract
----------------------
A team contract must have a clear and well defined goal due to a team consisting of multiple people with co-dependency and combined
resources that contribute to a common goal. The contract must define clear goals for the team members, who is responsible for what and
how will the team obtain its goal :cite:`teamContract`.

The team contract puts forth guidelines that all contribute to and goals that are accepted by all. Rules that define work environment and communications are especially important. A team that start their work together with a contract performs better than teams without a contract :cite:`teamContract`.


Trello - Cooperation Tool
------------------------------------------------
Trello is a collaboration tool for project organization and acts as a digital note board where each note contains description, photos, attachments and other documents to improve collaboration between team members. Trello helps with visualization of the tasks and sub-tasks and dependent features to reach the end goals by the project :cite:`what_is_trello`.
