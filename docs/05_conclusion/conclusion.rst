Conclusion
******************

The given task have been solved within the criteria for the project. The robot is able to harvest
the ripe strawberries in the defined strawberry field within the given time limit of 3 minutes.
Three consecutive tests were carried out which were all successfull and had a mean completion time
of 2 min 38 seconds, thus it can be concluded that the developed robot can reliably complete its specified task.
