.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Discussion
*****************

.. 
    During testing of the robot have the group used two of the provided plants for setup and tuning. Due to reducing the potential contact between other groups by using the same equipment and lack of access to the rest of the plants at the final setup and filming time the illustration of the strawberry field where made by moving the two plants at an already marked up field, as shown in :numref:`Figure %s <StrawberryField>`. At the current pase of the robot didn't this cause any problem. 

.. 
    Thing we will be graded for:
    • Overall impression of the work
    X Insight into product development
    • The use of digital projectplanning-, execution-and presentation-tools
    • Theoretical insight relevant to the assignment
    X Execution, including concept development
    X Result (prototype)
    * Reflection both on process, result and
    * Reflection on teamwork


Robot-Arm
----------------------

.. figure:: Pictures/TwoLink.png
    :name: twoLinks
    :scale: 40%
    :align: right
    :alt: nothing

    Two links

The robot navigates itself to a specified X- and Z-position from the strawberry. By making the robot itself handle the X- and Z-direction, the arm now 
only has to account for the height of the strawberry. It could be possible to integrate kinematics for Link 2 to improve the accuracy and functionality 
of the arm. By implementing rotation around the center of the wheel axis the robot arm is converted to a Two-Link arm and can pick strawberries closer to its body.
For controlling a Two-link solution its necessary to implement inverse kinematics. For easy calculation of the reference to the two links Denavit–Hartenberg parameters
can be used. Additionally, if possible, accessing the encoder in the continuous rotation servo motors used for the wheels would enable a positional algorithm of the robot
to be made so that the robot can maneuver away from the spotted strawberry position, and work as a link, see :numref:`Figure %s <twoLinks>`.


**Redesign** 

.. figure:: Pictures/NewArmDesign.png
    :name: newArmDesign
    :scale: 40%
    :align: left
    :alt: nothing

    Current arm design


The current design of the arm does not allow to pick up a strawberry from the ground due to an interference illustrated in :numref:`Figure %s <newArmDesign>`. A modification was manually made by removing part of the bed to reduce this restriction. 
This problem could be entirely fixed by moving the motor controlling the gripper a few millimeters in the Z-direction to increase the clearing between it and the mounting bed. 

**Gripper**

In this given case all the berries are of the same size and shape, which is taken into account for the gripper design. In the real world the berries would come in many shapes and sizes, which could give the gripper problem with grasping the berries. Also dependent on the genus of the strawberry the gripper could fit different to the average to one genus than another. The solution to this would be redesigning the gripper to be slightly larger, enclose the bottom end so that small strawberries don't fall out and change the chape so that the berries don't get stuck in the gripper when picked. This would vastly increase what kind of sized berries the robot could pick.

**Basket**

The basket is designed to be modular so it can easily be removed when emptying it for berries. To increase the level of automation of the strawberry field even further some sort of basket collection or emptying station would be needed. By placing throughout the field a couple of berry collection station, the robots could navigate to the closest station to deliver berries when the basket is full. These station could for instance lift off the basket when the robot are in position, and then eater replacing the basket with a new one, or emptying it before mounting it back on the robot. This would make it easier for the farmer, as they only need to collect all the berries from the collection station, and thereby decreasing the need to be out in the fields.


Robot Improvements
----------------------

To increase the robots accuracy to pick berries and navigate around in the field, a second camera could be implemented. This could make the robot more accurate, creating a 3D vision of the environment by the use of trigonometry, as well as adding some sort of redundancy for the vision if something happened to one of the cameras. Another alternative would be by adding a ultrasonic sensors, giving the robot more additional data to interpret the environment. This would increase the overall cost of producing the robot, but would also increase its capability.

All suggestions would modify some part of the robot to enhance its function and overall automation of the field. Some of these changes could lead to increase the end cost of the development and product delivered to the costumer, but could also create a vastly better end product with a higher grade of automation. The developed robot at its' current state do fulfill the costumers condition, but could be enhanced even more if the costumer wishes to increase the automation of the strawberry field. To which degree these changes and additions would be implemented should be discussed with the costumer to secure a end result that the costumer would be satisfied with.


Real World Implementation 
-----------------------------
For the test scenarios and methods mentioned a small robot was sufficient for the task at hand. In a real implementation the information from :numref:`Section %s <Competition Analysis>`. For the case of implementation the terrain, picking capacity and pick time would need to reevaluated to ensure best possible implementation. It would also be necessary to use the most environmental and least damaging material for any real world implementation. As mentioned in :numref:`Section %s <United Nations Goals and Ethical and Environmental impacts>` the use of plastic pollutes the environment and should only be used for indoor tests and be recycled afterwards.

Software Additions to Robot
---------------------------
It could also be recommended for an implementation to feature software that tracks the number of berries picked, number of green berries encountered, log the position of encountered berries and if they were picked or not. By collecting data the strawberry field can easily be analyzed to determine number of unripe berries and the location of said berries. Data analysis could also be used to implemented most efficient path and track the growth of strawberries.


ArUco Marker Detectability
---------------------------

The robot was not able to reliably detect the ArUco markers handed out in the MAS507 course at the starting position of
the track. Three solutions to this problem were identifed during testing of the robot.

**Upgrading the Camera Module**

Upgrading the camera module used to a higher specification camera could greatly increase the robots ArUco detection
capabilities. A higher resolution would make the ArUco legible at greater distances. A better lense would make the
ArUco detectable at lower ambient light situations. A camera with a high quality stabilization system would make the
robot able to detect ArUco markers more reliably during movement.

**Increasing Light**

By either increasing the ambient light or pointing lamps directly at the ArUco markers would increase the detecting
abilities of the camera.

**Increasing ArUco Marker Size**

Increasing the size of the ArUco markers would make them legible from greater distances. This solution was implemented in the
final solution to increase the detection reliability of the robot. Increasing the size from the original 11 cm marker
length to 16 cm greatly increased the robots detection capabilities at long range.


Mounted Camera Angle
---------------------------

The camera angle of the jetbot given by the MAS507 course was mounted at a approximately a negative 30 degree angle. This lead to
the posibility of the robot losing detection of a strawberry upon getting close enough to pick it. This was solved during the project
by slightly unscrewing the camera to make the camera instead face straight forward.