# Configuration file for the Sphinx documentation builder.
#
# Documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import re
import sphinx_rtd_theme
from datetime import datetime
import sys

# sys.path.insert(0, os.path.abspath('.'))

# -- Package Information -----------------------------------------------------
# python 2.7.18 64-bit
# sphinx 1.8.5

# -- Project information -----------------------------------------------------

project = 'MAS507 - Group 6'
copyright = 'Stian Andre Kristensen, Ole Henrik Haakstad, Håkon Tvilde, Tom Erik Vange'
author = 'Stian Andre Kristensen, Ole Henrik Haakstad, Håkon Tvilde, Tom Erik Vange'

# The full version, including alpha/beta/rc tags
release = re.sub('^v', '', os.popen('git describe').read().strip())

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'recommonmark',
    'sphinx_rtd_theme',
    'sphinx.ext.autosectionlabel',
    'sphinxcontrib.bibtex'
    ]

# Handle both ReST and Markdown files
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown'
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
numfig = True
html_theme_options = {
    'logo_only': True,
    'prev_next_buttons_location': 'bottom',
    'body_max_width': 'none'
}

html_logo = "00_globalFiles/websiteLogo.png"

html_context = {
"last modified": datetime.now().strftime('%d/%m/%Y'), #Adding last updated date, automatically when last build happened
}

#Removes the "Show Page Source button"
html_show_sourcelink = False

# Add any paths that contain custom static files (such as style sheets) here, relative to this directory.
# They are copied after the builtin static files, so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# These paths are relative to html_static_path
html_css_files = ['css/custom.css']

# -- Master Doc Setup --------------------------------------------------------------
# Pointing master doc to the correct document
master_doc = 'index'
