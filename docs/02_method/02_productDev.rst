.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Project Management
=====================================================

Teamwork
--------------------------------
As mentioned in :numref:`Section %s <Smarter and Better Teamwork>` the method to achieve better team dynamic and improve the ways members work together is to adhere to the "Eight Behaviors" and the assumptions and core values of the mutual learning mindset. During the first team meeting the "Eight Behaviors" and the "Assumptions of the Mutual Learning Mindset" was agreed to be followed by all members. 

Team Contract
-------------------------------------------------
A team contract was created in order to define each members desired learning outcome as well as relevant experience. The contract also made clear expectations to cooperation, regular meetings and day of meetings, team leader and method of communication. The team contract also lays out tools to be used by the team, such as Gitlab for programming and documentation, Trello-Board for cooperation and Messenger for communication.

The team contract is appended to this website in  :numref:`Chapter %s <Appendix A>`.

Cooperation Tool
---------------------------------------
As made clear in the team contract a Trello board is to be used as the cooperation tool. The advantages of a cooperation tool like Trello are mentioned in :numref:`Subsection %s <Trello - Cooperation Tool>`. Three lists was used for stage categorization:

    * TODO - Tasks that are to be done AND not started
    * IN PROGRESS - Tasks that are to be done AND started
    * DONE - Tasks that are completed

With the following tags used to describe type of task:

    * ProductDev - Tasks that are related to the development of the product
    * SoftwareDev - Tasks that are related to the programming of the jetbot
    * ProjectDev - Tasks that are related to the website used 

During the weekly meeting the trello board was updated with new tasks, progress of existing tasks and completed tasks. The trello board ensured a steady tracked progress as well as ensuring fair workload for all members.

Strawberry Field
=========================================================

.. figure:: Pictures/Field.png
    :name: Field
    :scale: 50%
    :align: right
    :alt: Strawberry field   

    Defined strawberry field.

The strawberry field consists of strawberries plants with each plant having two magnet-mounted berries. The field layout of plants and ArUco markers is shown in :numref:`Figure %s <Field>`. The total amount of plants in the field is 5, with a total of 8 ripe berries and 2 unripe berries. Due to the pandemic only a limited number of strawberry plants was available and thus the actual field consisted of 2 plants was moved during tests.

The dimensions of the strawberry field is shown in :numref:`Figure %s <Field>`, the 5 strawberry plants are located 15 cm from each other and 15 cm from the robot path. The aruco markers are located at a distance of 25cm from the path.

Product Analysis & Development
=================================

Competition Analysis
-----------------------------------
The market for strawberry pickers is still under development and as such there is a limit on competing products. The two solutions presented seems to be the most prominent solutions that cater to different markets. The products are compared in :numref:`Competition Comparison` although they cater to different markets.


.. figure:: Pictures/rubicon.jpg
    :name: rubiconRobot
    :scale: 80%
    :align: left
    :alt: Rubicon Robot

    Image of Rubicon Robot :cite:`rubiconImage`

**Rubicon** |br|
The picker produced by Octinion promises the ability to pick a ripe strawberry without any bruises as frequent as every 5 second, with a total picking ability between 180kg and 360kg of strawberries in 16-hours. The Rubicon robot manuevers with the use of wheels and identifies and picks strawberries separately such that only the ripe berries are harvested :cite:`octinion`. :numref:`Figure %s <rubiconRobot>` shows the Rubicon prototype.

**Berry 5** |br|

.. figure:: Pictures/B5.png
    :name: Berry5
    :scale: 60%
    :align: right
    :alt: Berry 5 robot

    Image of B5 Robot :cite:`crooImage`


The picker from "Harvest CROO Robotics" caters to consumers with the need of a large scale picker such as the Berry 5 robot, shown in Figure :numref:`Figure %s <Berry5>`. The B5 has a picking speed of 8 second per plant and can cover up to 8 acres of strawberry field in a single day :cite:`B5_robot`. 

|br|

Competition Comparison
-----------------------------------

The primary difference between the two products is the operating scale, where Berry 5 caters to the big scale farms while Rubicon caters to the small-medium scale farms. While each manufacturer does not list prize on their website there is still expected that the Berry 5 is more expensive due to its size.

The two robots also differ in their picking method as the Rubicon picks and places one berry at the time while the Berry 5 bot analyzes the whole plant before using a rotating arm to pick and hold berries before they are placed in the basket :cite:`B5_robot` :cite:`octinion`. The Octinion also require the growth platform for the berries to be elevated.

The main difference betweens the robots is that Berry 5 caters to a market consisting of large farm for commercial customers while the Octinion caters to small-medium farms in specialized cases due to the berries needs to be grown on elevated platforms. 

United Nations Goals and Ethical and Environmental impacts
------------------------------------------------------------------
The goals from United Nations Sustainable Development Goals that are most applicable for the strawberry picker;

2. Eradicate hunger. |br| 
By automate the process of harvesting crops could lead to a decrease of the price of the goods itself, since the farmer maybe don't need to hire workers to harvest, and thereby lowering the cost of the harvesting. 
This could also provide a opportunity for the farmers to expand and increase their fields, since the cost of harvest could be lowered, but also the plausibility of less crops loss or going to waste with the aid of data collection and monitoring. With implementing collection of data the state of the crops could be stated, as well as compering results across several years or between fields and farmers, finding the parameters that could increase the crops and the plant efficiency.

In this case the costumer want to automate the harvest of ripe strawberries from a small field. By the implementation of a basket collection station and implementation of several robots, the costumer could increase the field given that the condition are similar.


3. Better health and life quality. |br| 
By automating the harvest process, the need of pickers or harvesters could be decreased or removed. This could give the earlier harvesters a better work environment, seen from a ergonomics standpoint. These workers wouldn't need to spend most of their harvesting workday to crawl or squat in a field not setup for ergonomics. Their task could for instance be moved to processing of the harvest or looking after the robots. |br|

With a vast strawberry field a large proportion of the time would be spent bend down to the plants to harvest the ripe berries. With the automated berries collected shown for the costumer, the scale of the field could be increased, better the ergonomics and potentially overtime help to aid and secure the life quality for the pickers.

9. Industry, innovation and economical growth. |br| 
By implementing some more data collection into the automated harvesting, the loss of crops could be decreased and the quality could be increased. The farmers could also as mentioned decrease cost of hiring pickers or harvesters and thereby increase the overall profit of the farm. It could also result in less time consumption, since parts of the process would be automated, and thereby freeing up time that could be used to other work, innovation or increase leisure time.

These are just some example of what could be implemented and values to strive for, but could lead to grater impacts both locally where they could be implemented, but also globally if adapted by other firms and organizations.

12. Ensure Sustainable Consumption and Production Patterns |br| 
Worldwide consumption in water, energy and food must be so that resources consumed must not have a destructive impact on the planet :cite:`un_consumption_goal`. The plastic pollution problem is a significant issue in and currently pollution in river and lakes is greater than nature can repair and steps have to be taken in order to reduce pollution :cite:`national_geographic` :cite:`un_consumption_goal`. Any strawberry picker used in nature should not introduce additional plastic pollution, any picker implemented in nature should be made out of a material that is recyclable by nature or not harmful to nature. 

Product Specifications
-----------------------------------
A set of specifications was set in the beginning of the product development phase after identifying the customer needs. These specifications are only preliminary and subject to changes before a final set of specifications are set, the final specifications are covered in Section :numref:`Final Specifications`. The preliminary specifications are listed in Table :numref:`initProdSpec`.

.. csv-table:: Initial Product Specification
  :name: initProdSpec
  :header: Description, Could, Should, Must
  :widths: 25 25 25 25

  **Requirements/functions/features** |br| |br| Detect Strawberry |br| Distinguish ripe and non-ripe berries |br| Place berries in holding device  |br| Identify and save ripeness of berry, |br| |br| - |br| - |br| - |br| x, |br| |br| - |br| x |br| - |br| -, |br| |br| x |br| - |br| x |br| -
  **Parts and Production** |br| |br| Simple Production, |br| |br| -  , |br| |br| x , |br| |br| - 
  **cost** |br| |br| Software cost minimized |br| Part cost minimized, |br| |br| - |br| -, |br| |br| x |br| x, |br| |br| - |br| -


Concepts
-----------------------------------
Concepts are generated with the 5 step process mentioned in :numref:`Concept Generation` and reflection was applied to each step during the process and therefore not discussed as a single step.

**1. Clarify** |br|
The design challenge is to design a arm to pick a strawberry and place said berry in a basket or other storing device. No modifications are to be made to the jetbot chassis or to the location of the camera, as clarified by Associate Professor Tørdal :cite:`sondre_email`. 

The basket must be able to store 8 berries and handling of the berries should be not be squeezed, dropped or harmed in any way.

**2. External Search** |br|
During external search the existing solutions are found and used to generate concepts. Two existing solutions are mentioned in :numref:`Competition Analysis`.

**3. Internal Search** |br|
Internal search used the teams existing knowledge and creativity to create a concept. The method chosen to generate concepts was brainstorming as it is a common method that stimulates all team members and generated loads of ideas in a short time.

The data from the brainstorming session was compiled into a single chart for better visualization, shown in :numref:`Figure %s <brainstormThree>`.

.. figure:: Pictures/brainstorming.png
    :name: brainstormThree
    :scale: 25%
    :align: center
    :alt: Brainstorming Image   

    Brainstorming Image

**4.Systematic Exploration** |br|
After the brainstorming session and visualization all the data was systematically explored. Solutions and options deemed outside of the task was disregarded, such as removing the motor-size idea as the motors are supplied.

The number of solutions after the internal and external search was not deemed enough to use the "concept classification tree" or "concept combination table" tools mentioned in step 4 in :numref:`Concept Generation`.

**Concept 1**

.. figure:: Pictures/PrototypeOneWBasket.PNG
    :name: PrototypeOne
    :scale: 50%
    :align: left
    :alt: image 1   

    Concept 1 computer designed model

|br|

The first concept has a fixed arm mounted on a revolving base with pinching arm for picking the berries. A total of three motors are required, one required to rotating the base, one for picking the berries and one additional motor used for raising and lowering the arm. The basket for storing strawberries is mounted at the front. Concept 1 is shown in :numref:`Figure %s <PrototypeOne>`


|br|
|br|
|br|

.. 
    300m, 800g of material

.. 
    The first prototype suggestion consisted of an arm with a telescope extension mounted on top of the robot, as shown in :numref:`Figure %s <PrototypeOne>`. With this prototype the range for picking and placing strawberries would be grate, but would lead to a complex setup, use quite an amount of printing material, and would be potentially problematic to fine adjustment and control, since small adjustments on a motor close to the robot could result in vast movements at the end of the long arm.
    It could also demand other, larger electrical motors to operate, since with a fully extended arm will put a greater stress and demand on the motors and construction.
    The positive with this design is that the basket could potentially be mounted anywhere on the robot, and with the long range of the arm it could be used in other application. This concept would consume approximately 300 m(880 g) of printing material.

|br|

**Concept 2**

.. figure:: Pictures/PrototypeTwo.PNG
    :name: PrototypeTwo
    :scale: 50%
    :align: right
    :alt: image 1   

    Concept 2 computer designed model

The second concept is a fixed length arm using three motors. One for revolving the arm, one used to open and close the picking mechanism and one motor used to raise and lower the arm. The arm is mounted to the side so that the camera vision is not obstructed. The basket is mounted on the front, similar to concept 1, but with holes to reduce the weight. The second prototype is shown in :numref:`Figure %s <PrototypeTwo>`.

.. 
    50m, 150g

.. 
    The second proposal, shown in :numref:`Figure %s <PrototypeTwo>`, consist of a more compact setup mounted on the front of the robot. This setup would require less material than the first proposal, and with shorter arms would decrease forces acting on the motors and could help with the accuracy for picking the berries.
    The problems with this prototype could be the arms range dependent on the clients need considering the strawberry field and that the arm and basket could obscure parts of the camera view. To print this concept ca 50 m of printing material would be needed (150 g).

|br|
**Concept 3**

.. figure:: Pictures/PrototypeThreeWBasket.PNG
    :name: PrototypeThree
    :scale: 50%
    :align: left
    :alt: image 1   

    Concept 3 computer designed model

|br|

The third concept, :numref:`Figure %s <PrototypeThree>`, uses a telescopic arm mounted at a fixed angle with two motors required. One motor is for extending and retracting the telescopic arm and one motor for picking the berries. The basket is mounted under the arm such that the arm can release picked berries straight into the basket. The basket has an decline such that berries slide into the bottom of the basket and makes room for the next picked berry.

.. 
    200m,  600g
.. 
    :numref:`Figure %s <PrototypeThree>` shows the third proposal. Here the picking arm consist of a telescope solution which extends to collect the berries. The plan was then to place a basket underneath the arm. For the printing aspect this design would need moderate amount of printer material compared to the other designs.
    The problem with this where the limitation with the arm range for picking the berries. Since this robot is, the correction for the arm left and right would need to be taken by the robot driving left or right instead of the arm doing these adjustments as in the other prototypes.
    If the berries where above or below the grippers reach, the robot would not be able to pick the berries, and this function would need further improvements. For printing this rough concept would need 200 m of material (600g).
 
 

|br|
|br|
|br|
**Concept 4**

.. figure:: Pictures/PrototypeFour.PNG
    :name: PrototypeFour
    :scale: 50%
    :align: right
    :alt: image 1   

    Concept 4 computer designed model


Concept 4, shown in :numref:`Figure %s <PrototypeFour>`, is similar to concept 2 with a redesigned arm and basket solution. Similar to the other concepts the arm revolves around its base and has a motor for the picking the berries. The arm is mounted at a fixed angle and like concept 3 the arm drops the berry into the basket.

.. 
    64m, 191g

.. 
    Prototype four are partially similar to prototype two, except the collection basket are localized further back with a slide to guide the berries into the basket, shown in :numref:`Figure %s <PrototypeFour>`. Here a potentially bigger basket can be mounted at the rear of the robot and increasing its' carrying capacity. The challenge to take advantage of this extra carrying capacity would be to distribute the berries evenly in the basket. This concept would acquire approximately 64 m of material to print (191g).

|br|
|br|
**Concept 5**

.. figure:: Pictures/PrototypeFive.PNG
    :name: PrototypeFive
    :scale: 50%
    :align: left
    :alt: image 1   

    Concept 5 computer designed model

|br|
The fifth concept has the basket with teeth mounted at the end of two arms. The basket functions both as the picking mechanism and the basket for storing the berries. The motor for raising and lowering the basket is mounted at where the arms is mounted to the robot. The concept is shown in :numref:`Figure %s <PrototypeFive>`.

.. 
    35m, 105g

|br|
|br|
|br|
|br|

Final Specifications
-----------------------------------
With the knowledge gathered in the concept-generation phase the specifications are refined and narrowed in advance to the evaluation of the concepts, :numref:`Figure %s <Concept Evaluation>`. Any *could* requirement is re-assigned to "should/must" or entirely dropped. Criteria that are added/modified/removed are listed below: 

    * Identify and save ripeness of berry - Dropped as it is purely software and can be incorporated once a prototype is developed.
    * A specific criteria to the number of motors is added.
    * A criteria added to the accuracy of the arm and the picking sensitivity.
    * A criteria to the mass of the arm/basket.
    * A Criteria to the center of mass location of the arm and basket, relative to the robot.

.. csv-table:: Final Product Specification
  :name: finitProdSpec
  :header: Description, Should, Must
  :widths: 25 25 25

  **Requirements/functions/features** |br| |br| Detect Strawberry |br| Distinguish ripe and non-ripe berries |br| Place berries in holding device |br| Center of mass close to the robot , |br| |br| - |br| x |br| - |br| -, |br| |br| x |br| - |br| x |br| -
  **Parts and Production** |br| |br| As few motors as possible |br| Arm/Basket Combined Mass |br|  Simple production methods, |br| |br| x |br| x, |br| |br| - |br| -
  **cost** |br| |br| Software cost minimized |br| Part cost minimized, |br| |br| x |br| x, |br| |br| - |br| -
  **Certifications** |br| |br| Safety = no demands |br| Standards = no requirements 



Concept Evaluation
-----------------------------------

The concept evaluation results are listed in :numref:`Table %s <conceptEvaluationofConcepts>` with :numref:`Table %s <costEvaluationConcepts>` showing the cost evaluation. Each criteria is given weight by their importance by the team as a whole. The criterias that contribute negatively are given a negative value such that the highest score is the best theoretical concept.  

A significant weight was to arm accuracy due to the observation that relying on the jetbot to rotate the arm significantly reduces the accuracy. Achieving a correct theoretical "accuracy versus number of motors" weight balance was deemed to challenging and therefore this balance was analyzed manually.

.. 
    Concepts that doesn't use a third motor heavily rely on using the jetbot to rotate around its own axis. Using the jetbot to rotate has the obvious advantage of reducing the required motors by one but introduces accuracy errors. Achieving a correct theoretical "accuracy versus numbers of motors" weight balance was deemed to difficult and therefore evaluated manually. Concepts that used an additional motors resulted in significant greater accuracy.


.. csv-table:: Evaluation
  :name: conceptEvaluationofConcepts
  :header: Criteria, Weight, Concept 1, Concept 2, Concept 3, Concept 4, Concept 5, Ideal
  :widths: 25 25 25 25 25 25 25 25

  *Function Dimension* |br| |br| Capacity  |br| Number of motors |br| Arm Maneuverability |br| Accuracy , |br| |br| 0.2 |br| -1.0 |br| 4.0 |br| 1.0, |br| |br| 8 |br| 3 |br| 2 |br| 2 |br|, |br| |br| 8 |br| 3 |br| 2 |br| 2, |br| |br| 8 |br| 2 |br| 1 |br| 1, |br| |br| 10 |br| 2 |br| 1 |br| 1, |br| |br| 6 |br| 1 |br| 1 |br| 0, |br| |br| 8 |br| 1 |br| 3 |br| 3
  *Production Dimension* |br| |br| Picking Sensitivity |br| Simple Production, |br| |br| 0.8 |br| 0.3, |br| |br| 0.5 |br| 1, |br| |br| 4 |br| 3, |br| |br| 0.5 |br| 1, |br| |br| 4 |br| 3, |br| |br| 3 |br| 4, |br| |br| 4 |br| 5 
  *Design Dimension* |br| |br| Material Usage |br| Center of mass location, |br| |br| -0.1 |br| 0.5, |br| |br| 8 |br| 7, |br| |br| 1.5 |br| 3, |br| |br| 6 |br| 10, |br| |br| 1.9 |br| 5, |br| |br| 1.1 |br| 1, |br| |br| 1 |br| 10
  Sum |br| Relative Value, ,10 |br| 0.5, 12.1 |br| 0.6, 8.7 |br| 0.4, 10 |br| 0.5, 8.2 |br| 0.4, 22.2 |br| 1.0

.. csv-table:: Cost Evaluation
  :name: costEvaluationConcepts
  :header: Criteria, Weight, Concept 1, Concept 2, Concept 3, Concept 4, Concept 5, Ideal
  :widths: 25 25 25 25 25 25 25 25

    *Product Dimension* |br| |br| Material Usage |br| Number of motors, |br| |br| 0.5 |br| 2, |br| |br| 80 |br| 3, |br| |br| 15 |br| 3, |br| |br| 60 |br| 2, |br| |br| 19 |br| 2, |br| |br| 11 |br| 1, |br| |br| 10 |br| 1
    Sum |br| Relative Value, , 46 |br| 6.6, 13.5 |br| 1.9, 34 |br| 4.9, 13.5 |br| 1.9, 7.5 |br| 1.1, 7 |br| 1

Concept Selection
-----------------------------------

In the evaluation concept 2 had the highest score of 12.1 and has the second lowest cost. It was deemed acceptable to create the concept with plastic as none of the test are in nature. The material use will have to be reconsidered for any real world implementation of the strawberry picker.

:numref:`Section %s <Prototyping>` covers the stages that the concept goes trough during the prototype phase.