Website Setup
=================================

This website was created with the intent of documenting and presenting the work achieved on the strawberry robot, the website is based on the same template used by Associate Professor Sondre Tørdal in the MAS507-course. Best practice has been followed during the website development by separating development and master branches :cite:`gitflow`. 

The local development of the website was made possible with the use of "sphinx" and "sphinx-autobuild". Extensions and setup are mostly as used by the mas507 repository [#]_ with a few added packages and modified settings:

    * Autosection-label installed to automatically numerate Tables/Figures
    * Bibtex extension installed to enable support citation and a numerated bibliography.
    * Custom CSS-code that sets a maximum site width to 1400px. Added for consistency across multiple developers.
    * Added a logo and removed the search box.

.. [#] The repository was forked September 1st