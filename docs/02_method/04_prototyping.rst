.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Prototyping
====================

As mentioned in :numref:`Concept Selection` concept 2 was selected and this section will cover the actual prototyping and making of the concept.

Design
------------------

.. figure:: Pictures/PrototypeTwoRedefined.PNG
    :name: PrototypeTwoV1
    :scale: 65%
    :align: right
    :alt: image 1   

    Second concept draft.

Firstly the accurate dimensions of motors and the jetbot was added to the CAD-model. The updated model of the concept is shown in :numref:`Figure %s <PrototypeTwoV1>`.

Production
------------------
The use of 3D-printers allowed for a rapid process in the creation of prototypes. To best possible adhere to United Nations goal 12; *Responsible Consumption and Production* the 3D-printer settings have been configure to use the least amount of material without affecting the sturdiness of the prototype.
 

Prototype V1.0
-----------------------------------------
After the production of the first prototype the issues not spotted during the design- and concept-phase became apparent. The first issue is that of mounting plate, which is used to designed to attach the motor and basket to the jetbot was designed with subpar clearances. This resulted in a unstable arm and basket during operation. The second issue was with the single-point basket mounting method. The single mounting point resulted in shaking of the berries during operation and meant that all the weight of the basket is carried at the front. Images of prototype 1 is shown in :numref:`Table %s <prototype1_images>`


.. csv-table:: Prototype 1
  :name: prototype1_images
  :widths: 25 25 25 

     .. figure:: Pictures/Basket.jpg, .. figure:: Pictures/FrontPlateV1.jpg, .. figure:: Pictures/ArmV1Part1.jpg
    Basket v1.0, Frontplate v1.0, Arm v1.0

Prototype V2.0 
-------------------------------------
In addition to the changes mentioned in :numref:`Subsection %s <Prototype V1.0>` the size and design of the basket was modified. The basket was updated to feature a decline such that the berries more softly landed in the basket. The size of the basket was also increased to accommodate the secondary mounting method.  The arm was made lighter and motor mounts was stiffened to add structural integrity.  

:numref:`Table %s <prototype2_images>` shows the re-designed basket, frontplate and arm. :numref:`Table %s <assemblyBasket>` shows the design, production and assembly of the new basket. Prototype 2.0 ended up being the final prototype of concept 2 as the basket, arm and mounting methods are all sufficient for the strawberry robot.

.. csv-table:: Prototype 2
  :name: prototype2_images
  :widths: 25 25 25 

     .. figure:: Pictures/BasketV2Assembled.jpg, .. figure:: Pictures/FrontplateV2.jpg, .. figure:: Pictures/ArmAssemblyV2.jpg
    Basket v2.0, Frontplate v2.0, Arm v2.0


.. csv-table:: Basket from design to assembly
  :name: assemblyBasket
  :widths: 25 25 25 

     .. figure:: Pictures/PrintBasketAssembly2.PNG, .. figure:: Pictures/BasketPrinted.jpg, .. figure:: Pictures/BasketV2Assembled.jpg
    Berry basket V2.0 ready for printing.,Printed basket V2.0 ready for assembly., Assembled basket.
