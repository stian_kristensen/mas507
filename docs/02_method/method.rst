Method
*********************************

.. include:: 03_website.rst
.. include:: 01_cameraVision.rst
.. include:: 02_productDev.rst
.. include:: 04_prototyping.rst
.. include:: 05_strawberryDetection.rst
.. include:: 06_armKinematics.rst