Robot Arm Kinematics
=============================

The robot drive towards the strawberry and positioning itself so the berry is centered in X-direction, shown in :numref:`Figure %s <Kinematicxyz>`, and in range of the gripper in Z-direction. Servo motor nr.2, which is position on the base, rotates the arm so the gripper can reach the berry. This is done by calculating the strawberry position, from the ground. Measurements in Equation the angle equation refers to :numref:`Figure %s <Kinematicxyz>`.


.. figure:: Pictures/KinematicXYZ.png
    :name: Kinematicxyz
    :scale: 45%
    :align: center
    :alt: nothing

    Kinematic in YZ-plane (left) and XZ-plane (right)


.. math::
    \theta = asin(\frac{berry_y - offset_{ground}}{armlength})



.. figure:: Pictures/BerryGripper.png
    :name: berryGripper
    :scale: 50%
    :align: right
    :alt: nothing

    Fully open strawberry gripper.

The strawberry is position in the center when the gripper is fully open, see :numref:`Figure %s <berryGripper>`.