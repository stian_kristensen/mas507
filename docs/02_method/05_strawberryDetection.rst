.. # define a hard line break for HTML
.. |br| raw:: html

   <br />

Strawberry Detection
================================

The strawberry detection script, used to find the strawberry and return its position, was provided by the lecturer from MAS507 – Product development. This script uses the python library OpenCV and is based on color recognition to find strawberries and PnP algorithm to return the strawberries distances and positions. The script is based on the strawberry detection script by the Github user“andridns” :cite:`strawGithubResposityory`.


Color Recognition
===========================

.. figure:: Pictures/color_scale.png
    :name: HSV_colorSpace
    :scale: 80%
    :align: right
    :alt: nothing

    HSV Color Space :cite:`hue_sv_colors_thinggy`.

Image information from Cameras is usually in the RGB color space. But in Color recognition it useful to convert to HSV color space. HSV separates color from illumination, and conversion is simple. 

Red must be defined two places in the HSV color space, as seen in :numref:`Figure %s <HSV_colorSpace>`. Red has values from 0 to 10 hue, and 170 to 180 hue. Both ranges must be defined separately and after added together to make one mask. This case only applies to red, since the HSV color space is cylindrical and is converted to 2-dimensional plan ref figure. 
High quality images contain details which can, in edge detection cases, be considered noise. This is fixed by blurring the image, which reduced the details.   In the strawberry detection script, the images are blurred with the gaussian blur function in OpenCV. 

|br| |br| |br|

.. figure:: Pictures/strawberries_img_processing.png
    :name: strawberryIMGProcc
    :scale: 100%
    :align: center
    :alt: nothing

    Strawberry mas without cleanup (left) vs with cleanup (right) :cite:`strawGithubResposityory`.


The mask cleanup uses the functions from OpenCV “morphologyEx” which can manipulate image shapes, as seen in :numref:`Figure %s <strawberryIMGProcc>`. The mask noise in the form of small white dots on the picture, and small holes inside the mask which does not make it complete. These noises can make the edge difficult :cite:`openCV_morph`. The strawberries in this project are uniform and the script should only return the position of the closest berry. This is done by the function “find_biggest_contour” which uses OpenCV function “findContours” and calculating every closed-contour area and keeps the largest one. 

Modifications
---------------------------------
Some modification was made to the color recognition algorithm. The original script had set the HSV color spectrum to detect a wide verity of reds, and detection errors was present with bad lighting and presence of bright reds. Cables, hands and different shades of red was also detected. This was fixed by changing the saturation, closer to the color of the strawberry. 

PnP
=============
The PnP algorithm can be solved by using OpenCV “solvePnP” function. The function passes through the camera calibration Intrinsic, camera points, and object points. 
The image points are found by calculating the strawberry masks center of mass and using that point as image point. Object point, the strawberry, must be measured by hand to give the algorithm a reference point. 

