
Appendices
************

Appendix A
=================

:download:`Team Contract <Team_Contract.pdf>`


Appendix B
====================

|location_link_1|

.. |location_link_1| raw:: html

   <a href="https://youtu.be/BtGbk9CoIWc" target="_blank">Video of 1st test.</a>

|location_link_2|

.. |location_link_2| raw:: html

   <a href="https://youtu.be/g6_XBzYqX2g" target="_blank">Video of 2nd test.</a>

|location_link_3|

.. |location_link_3| raw:: html

   <a href="https://youtu.be/L3Gtypap0Ts" target="_blank">Video of 3rd test.</a>


Appendix C
=================
This appendix includes all the python code used in the Yeetbot.

calibrateCamera.py
-------------------------------------

.. code-block:: python

    # Imports
    import cv2
    import pathlib
    import numpy as np

    # Set a folder for saving images
    imageLocation = pathlib.Path("./images")

    # Shape of chessboard
    sh1 = 6
    sh2 = 8

    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # Make object point arrays, used by camera calibration function
    objp = np.zeros((sh1 * sh2, 3), np.float32)
    objp[:, :2] = np.mgrid[0:sh1, 0:sh2].T.reshape(-1, 2)

    # Initialize list
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    # Find all images in path
    filenames = imageLocation.glob("*.png")
    for filename in filenames:
        # Read image
        img = cv2.imread(str(filename))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find chess board corners
        ret, corners = cv2.findChessboardCorners(gray, patternSize=(sh1, sh2))

        if ret == True:
            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
            imgpoints.append(corners2)
        else:
            print("Image %s trashed" % (str(filename)))

    # Calibrate camera
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
        objpoints, imgpoints, gray.shape[::-1], None, None
    )

    # Save calibration data
    calibrationPath = imageLocation.joinpath("intrinsicCalibration.npz")
    np.savez(str(calibrationPath), mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)


imageSaver.py
-------------------------------------

.. code-block:: python

    #!/usr/bin/env python
    """
    Node for saving Raw Jetbot images
    """
    import os
    import glob
    import rospy
    import cv2
    from cv_bridge import CvBridge, CvBridgeError
    from sensor_msgs.msg import Image
    from std_msgs.msg import Bool

    class ImageSaver(object):
        idx = 0

        def __init__(self):
            self.imagePath = '%s/catkin_ws/src/mas507/calibration/images/' % (os.path.expanduser("~"))
            self.cvBridge = CvBridge()

            
            if not os.path.isdir(self.imagePath):
                # Ensure folder exist and create folder if non-existing
                os.mkdir(self.imagePath)

            else:
                # Delete content if folder exist
                files = os.listdir(self.imagePath)

                for f in files:
                    os.remove(os.path.join(self.imagePath, f))


        def callback(self, msg):
            if msg.data:
                # Read RAW image
                image = rospy.wait_for_message('/image_raw', Image)

                # Try to save image
                try:
                    cv_image = self.cvBridge.imgmsg_to_cv2(image)
                    filename = self.imagePath + "image" + str(self.idx) + ".png"
                    cv2.imwrite(filename, cv_image)

                except CvBridgeError as e:
                    print(e)

                # Increment image counter
                self.idx += 1

                # Reset ROS trigger message
                out = Bool()
                out = False
                pub.publish(out)

                # Print info to console
                print("Saved: " + filename)


    if __name__ == '__main__':
        try:
            # Initialize node
            rospy.init_node('imageSaver')

            # Publisher
            pub = rospy.Publisher('saveImage', Bool, queue_size=1)

            # Image saving class
            imageSaver = ImageSaver()
            
            # Subscribers
            rospy.Subscriber("saveImage", Bool, imageSaver.callback)

            # Keep node alive
            rospy.spin()

        except rospy.ROSInterruptException:
            pass


MainController.py
-------------------------------------

.. code-block:: python

    #!/usr/bin/env python
    """
    Main Control Node
    """
    import rospy
    import numpy as np
    import os
    from mas507.msg import ServoTargets, WebJoystick, ArucoData, ServoSetpoints
    from sensor_msgs.msg import Image
    from StrawberryDetector import StrawberryDetector
    from YeetbotController import Yeetbot, Joystick, PickStateMachine, servo1_threshold, servo2_threshold, servo3_threshold, servo2_range, leftWheelIdle, rightWheelIdle, initialServos
    from math import sqrt
    import time


    def arucoCallback(msg): # read aruco data from messages
        global aruco_x
        global aruco_y
        global aruco_z
        global aruco_id

        aruco_x = msg.x
        aruco_y = msg.y
        aruco_z = msg.z
        aruco_id = msg.id

    def servoCallback(msg): # read data from ServoSetpoints message
        global servos
        
        servos = [msg.servo1, msg.servo2, msg.servo3]

    if __name__ == '__main__':
        try:
            # Init ROS node
            rospy.init_node('mainController', anonymous=True)

            # Init web joysticks
            leftJoystick = Joystick()
            rightJoystick = Joystick()

            # Init Yeetbot
            servoTargets = ServoTargets()
            yeetbot = Yeetbot()
            servos = initialServos[:]
            pickStateMachine = PickStateMachine()
            yeetbot.moveSpeed = 10 # move speed of yeetbot
            

            # Init Aruco markers coordinates
            aruco_x = 9999
            aruco_y = 9999
            aruco_z = 9999
            aruco_id = -1

            # Init various variables for control loop
            aruco_id_previous = [10, 10] #storage for previous aruco markers, important that the initial values differs from any available ID
            statusMessage = 'Initializing Yeetbot...'
            statusMessagePrevious = ' '
            arucoCounter = 0 # number of aruco markers found

            # User controllable variables
            joystickControl = False
            arucoStopDistance = 240 # [mm] used to stop the yeetbot when it gets within a certain range of an aruco marker
            strawberryPickDistance = 220 # [mm] minimum threshold to start strawberry picking sequence
            strawberryMinDistance = strawberryPickDistance + 30 # [mm] minimum distance to start moving towards a strawberry
            turnTime = 1.2 # [s] time to turn when searching
            turnSpeed = 6 # velocity value used for turning
            moveSpeedAruco = 12 # velocity value used when following an aruco marker
            moveSpeedStrawberry = 10 # velocity value used when following a strawberry
            
            
            # Publishers
            pub_servoTargets = rospy.Publisher('servoTargets', ServoTargets, queue_size=1)
            pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)

            # Strawberry detector
            intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
            strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])
                


            # Subscribers
            sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)
            sub_leftJoystick = rospy.Subscriber('webJoystickLeft', WebJoystick, leftJoystick.callback)
            sub_rightJoystick = rospy.Subscriber('webJoystickRight', WebJoystick, rightJoystick.callback)
            sub_arucoData = rospy.Subscriber('arucoData', ArucoData, arucoCallback)
            sub_servoSetpoints = rospy.Subscriber('servoSetpoints', ServoSetpoints, servoCallback)
            
            
            # Start Synchronous ROS node execution
            rate = rospy.Rate(10)
            
            #yeetbot.state = yeetbot.state_test
            

            # Control Loop
            while not rospy.is_shutdown():

                # Status print
                if statusMessage != statusMessagePrevious: #only print when a new message is queued
                    print(statusMessage)
                    statusMessagePrevious = statusMessage
                
                
                # find aruco
                if yeetbot.state == yeetbot.state_findAruco:
                    statusMessage = 'Searching for ArUco...'
                    
                    # checks if a new aruco is found
                    if yeetbot.turnRight(turnTime, turnSpeed) == True and aruco_id != -1 and aruco_id != aruco_id_previous[0] and aruco_id != aruco_id_previous[1]: 
                        statusMessage = 'ArUco found!'
                        aruco_id_previous[0] = aruco_id_previous[1]
                        aruco_id_previous[1] = aruco_id
                        arucoCounter = arucoCounter + 1
                        yeetbot.state = yeetbot.state_move2aruco
                    
                    # stops robot if all aruco markers have been found
                    elif arucoCounter >= 3:
                        yeetbot.state = yeetbot.state_stop
                    
                
                # move to aruco
                elif yeetbot.state == yeetbot.state_move2aruco:
                    statusMessage = 'Moving to ArUco...'
                    yeetbot.followTarget(aruco_x, moveSpeedAruco)
                    distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

                    # checks if yeetbot is within minimum distance of a strawberry
                    if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance and arucoCounter != 2:
                        statusMessage = 'Strawberry found!'
                        yeetbot.state = yeetbot.state_move2strawberry

                    # checks if yeetbot has reached stop-distance from aruco marker
                    elif aruco_z <= arucoStopDistance and aruco_id != -1: 
                        statusMessage = 'ArUco distance limit reached.'
                        yeetbot.stop()
                        yeetbot.state = yeetbot.state_findAruco

                    # returns to aruco if aruco id is lost
                    elif aruco_id == -1:
                        statusMessage = 'ArUco lost!'
                        yeetbot.state = yeetbot.state_return2aruco


                # move to strawberry
                elif yeetbot.state == yeetbot.state_move2strawberry:

                    if strawberryDetector.exist == 1:
                        statusMessage = 'Moving towards the strawberry...'
                        distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

                        # ensures that the yeetbot is neither too far away nor too close to the strawberry
                        if distance2strawberry > strawberryPickDistance:
                            yeetbot.followTarget((strawberryDetector.x)*2, moveSpeedStrawberry)

                        else:
                            yeetbot.backAwayFromTarget((strawberryDetector.x)*2, (moveSpeedStrawberry+5))
                        
                        # check if strawberry is in the center of the camera-view and within acceptable range
                        if abs(strawberryDetector.x) <= 35 and (strawberryPickDistance - 6) <= distance2strawberry <= (strawberryPickDistance+6):
                            statusMessage = 'Strawberry reached.'
                            yeetbot.stop()
                            yeetbot.state = yeetbot.state_pickStrawberry
                            
                    else:
                        statusMessage = 'Strawberry lost!'
                        yeetbot.state = yeetbot.state_return2track


                # pick strawberry
                elif yeetbot.state == yeetbot.state_pickStrawberry: 
                    
                    # saves the detected y value of the strawberry as it will be obstructed from view when picking
                    if pickStateMachine.state == pickStateMachine.saveStrawberryHeight and strawberryDetector.exist == 1:
                        savedStrawberryY = strawberryDetector.y
                        pickStateMachine.state = pickStateMachine.targetStrawberry


                    # moves arm to the target strawberry
                    elif pickStateMachine.state == pickStateMachine.targetStrawberry:
                        statusMessage = 'Targeting strawberry...'

                        if yeetbot.targetStrawberry(savedStrawberryY, servos) == True:
                            pickStateMachine.state = pickStateMachine.envelopStrawberry

                    # moves forward towards the strawberry
                    elif pickStateMachine.state == pickStateMachine.envelopStrawberry:
                        statusMessage = 'Extracting strawberry...'

                        if yeetbot.moveForward(1.3, 6) == True:
                            pickStateMachine.state = pickStateMachine.catchStrawberry

                    # closes hand around the strawberry
                    elif pickStateMachine.state == pickStateMachine.catchStrawberry:

                        if yeetbot.closeArm(servos) == True:
                            pickStateMachine.state = pickStateMachine.backAway

                    # backs away from the strawberry
                    elif pickStateMachine.state == pickStateMachine.backAway:

                        if yeetbot.moveBackward(1.1, 30) == True:
                            pickStateMachine.state = pickStateMachine.move2basket
                        
                    # moves the strawberry to the basket
                    elif pickStateMachine.state == pickStateMachine.move2basket:
                        statusMessage = 'Moving strawberry to basket...'

                        if yeetbot.move2basket(servos) == True:
                            pickStateMachine.state = pickStateMachine.release

                    # releases the strawberry
                    elif pickStateMachine.state == pickStateMachine.release:
                        
                        if yeetbot.openArm(servos) == True:
                            pickStateMachine.state = pickStateMachine.reset
                        

                    # resets the arm
                    elif pickStateMachine.state == pickStateMachine.reset:
                        statusMessage = 'Resetting arm...'

                        if yeetbot.resetArm(servos) == True:
                            statusMessage = 'Arm successfully reset.'
                            pickStateMachine.state = pickStateMachine.check
                    
                    # checks if the strawberry was picked successfully
                    elif pickStateMachine.state == pickStateMachine.check:
                        
                        if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                            statusMessage = 'Strawberry picking failed!'
                            pickStateMachine.resetState()
                            yeetbot.state = yeetbot.state_move2strawberry

                        else:
                            statusMessage = 'Strawberry picked successfully!'
                            pickStateMachine.resetState()
                            yeetbot.state = yeetbot.state_return2aruco
                        
                        
                # return to track
                elif yeetbot.state == yeetbot.state_return2track:
                    statusMessage = 'Returning to track...'

                    if yeetbot.moveBackward(1, 15) == True:

                        if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                            yeetbot.state = yeetbot.state_move2strawberry

                        else:
                            yeetbot.state = yeetbot.state_return2aruco
                        

                # return to aruco
                elif yeetbot.state == yeetbot.state_return2aruco:
                    statusMessage = 'Searching for previous ArUco...'

                    if yeetbot.turnLeft(turnTime, turnSpeed) == True and aruco_id == aruco_id_previous[1]: # checks if the previous aruco is detected
                        statusMessage = 'Previous ArUco found!'
                        yeetbot.state = yeetbot.state_move2aruco 
                
                # stop yeetbot
                elif yeetbot.state == yeetbot.state_stop:
                    statusMessage = 'Operation finished.'
                    yeetbot.stop()
                        


                
                # Servo setpoints
                servoTargets.leftWheel  = yeetbot.leftWheel
                servoTargets.rightWheel = yeetbot.rightWheel

                # overwrites wheel servo values if joystick control is enabled
                if joystickControl == True:
                    servoTargets.leftWheel  = leftWheelIdle - rightJoystick.y/2 + leftJoystick.x/7
                    servoTargets.rightWheel = rightWheelIdle + rightJoystick.y/2 + leftJoystick.x/7
                

                servoTargets.servo1_target = yeetbot.servoTargets[0]
                servoTargets.servo2_target = yeetbot.servoTargets[1]
                servoTargets.servo3_target = yeetbot.servoTargets[2]
                
                # publishes servo setpoint values
                pub_servoTargets.publish(servoTargets)


                # Sleep remaining time
                rate.sleep()


        except rospy.ROSInterruptException:
            pass


StrawberryDetector.py
-------------------------------------

.. code-block:: python

    import cv2
    from cv_bridge import CvBridge
    import numpy as np



    # Inspired from: https://github.com/andridns/cv-strawberry
    class StrawberryDetector(object):
        def __init__(self, ros_publisher, mtx, dist):
            self.cvBridge = CvBridge()
            self.ros = ros_publisher
            self.mtx = mtx
            self.dist = dist
            self.x = 9999
            self.y = 9999
            self.z = 9999
            self.exist = -1 # == 1 if strawberry is detected


        def callback(self, msg):
            # Read image message and convert to CV image
            image = self.cvBridge.imgmsg_to_cv2(msg)

            # Convert from BGR to RGB
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # Blur image slightly
            image_blur = cv2.GaussianBlur(image, (7, 7), 0)

            # Convert to HSV
            image_blur_hsv = cv2.cvtColor(image_blur, cv2.COLOR_RGB2HSV)

            # 0-10 hue
            min_red = np.array([0, 150, 80])
            max_red = np.array([10, 256, 256])
            image_red1 = cv2.inRange(image_blur_hsv, min_red, max_red)

            # 170-180 hue
            min_red2 = np.array([170, 150, 80])
            max_red2 = np.array([180, 256, 256])
            image_red2 = cv2.inRange(image_blur_hsv, min_red2, max_red2)

            # Create red image
            image_red = image_red1 + image_red2

            # Fill small gaps and remove specks
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
            image_red_closed = cv2.morphologyEx(image_red, cv2.MORPH_CLOSE, kernel)
            image_red_closed_then_opened = cv2.morphologyEx(image_red_closed, cv2.MORPH_OPEN, kernel)

            # Find biggest red countour
            big_contour, red_mask = self.find_biggest_contour(image_red_closed_then_opened)

            if (big_contour is not None) and (red_mask is not None):
                # Centre of mass
                moments = cv2.moments(red_mask)
                centre_of_mass = int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])

                # Add poitn of COM in image
                cv2.circle(image, centre_of_mass, 5, (0, 255, 0), -1, cv2.LINE_AA)

                # Fit ellipse to detection
                ellipse = cv2.fitEllipse(big_contour)
                cv2.ellipse(image, ellipse, (0, 255, 0), 1)

                # Strawberry radius based on ellipse fitting
                r = int(ellipse[1][0]/2)

                # Add circle to image around COM and image boundary using calculated radius
                cv2.circle(image, centre_of_mass, r, (0, 0, 255), 1, cv2.LINE_AA)
                
                # PnP for strawberry location
                n = 30
                t = np.linspace(0, 2*np.pi, n)

                # 3D points of strawberry major circkle
                radius_strawberry = (32.0/1000.0)/2
                objpnts = np.zeros([n,3])
                objpnts[:,0] = radius_strawberry*np.sin(t)
                objpnts[:,1] = radius_strawberry*np.cos(t)

                # 2D points in image of detected strawberry major circle
                imgpoints = np.zeros([n,2])
                imgpoints[:,0] = centre_of_mass[0] + r*np.sin(t)
                imgpoints[:,1] = centre_of_mass[1] + r*np.cos(t)
                
                # Solve for position and orientation
                _, rvec, tvec = cv2.solvePnP(objpnts, imgpoints, self.mtx, self.dist)
                
                # Return detection results to class
                self.x = int((tvec[0, 0])*1000)
                self.y = -int((tvec[1, 0])*1000) + 50 #modify to represent height from floor at picking distance
                self.z = int((tvec[2, 0])*1000)
                self.exist = 1
            else:
                self.exist = -1

            


            # Publish image with detection
            image_bgr = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            self.ros.publish(self.cvBridge.cv2_to_imgmsg(image_bgr, "bgr8"))
            
            
        def find_biggest_contour(self, image):
            # Copy to prevent modification
            image = image.copy()
            contours, hierarchy = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

            # Isolate largest contour
            contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]

            # Return results
            if len(contour_sizes) != 0:
                biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
        
                mask = np.zeros(image.shape, np.uint8)
                cv2.drawContours(mask, [biggest_contour], -1, 255, -1)

            else:
                biggest_contour = None
                mask = None

            return biggest_contour, mask


WebController.py
-------------------------------------

.. code-block:: python
    
    #!/usr/bin/env python
    """
    Node for hosting WebController application

    Inspiration for this node:
    https://medium.com/@aqib.mehmood80/wireless-video-surveillance-robot-using-raspberry-pi-9caadd3eb30b
    """
    import rospy
    import cv2
    from cv_bridge import CvBridge
    from sensor_msgs.msg import Image
    from mas507.msg import WebJoystick
    import threading
    from flask import Flask, render_template, Response, request
    import time

    # Start ROS node in seperate thread
    threading.Thread(target=lambda: rospy.init_node('WebController', disable_signals=True)).start()

    # Create a Flask Controller App
    app = Flask(__name__)

    class RosImage(object):
        def __init__(self):
            self.bridge = CvBridge()
            self.frame = None

        def callback(self, msg):
            # Convert ROS image to bytes
            cv_image = self.bridge.imgmsg_to_cv2(msg)
            self.frame = cv2.imencode('.jpg', cv_image)[1].tobytes()

        def gen(self):
            while True:
                if self.frame is not None:
                    yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + self.frame + b'\r\n')

                time.sleep(0.05)
                
    # Create RosImage class which handles the ROS image callabacks
    rosImage = RosImage()
    sub_markers = rospy.Subscriber("image_markers", Image, rosImage.callback)

    # Joystick publishers
    pub_joyLeft = rospy.Publisher('webJoystickLeft', WebJoystick, queue_size=1)
    pub_joyRight = rospy.Publisher('webJoystickRight', WebJoystick, queue_size=1)



    # Route creations
    @app.route('/')
    def index():
        """Home page."""
        return render_template('index.html')

    @app.route('/video_feed')
    def video_feed():
        """Video streaming route."""
        # return Response(gen(Camera()), mimetype='multipart/x-mixed-replace; boundary=frame')
        return Response(rosImage.gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

    @app.route('/left_stick')
    def left_stick():
        # Get joystick data
        joystick = WebJoystick()

        joystick.x = int(request.args.get('x'))
        joystick.y = int(request.args.get('y'))

        pub_joyLeft.publish(joystick)

        return render_template('index.html')

    @app.route('/right_stick')
    def right_stick():
        # Get joystick data
        joystick = WebJoystick()

        joystick.x = int(request.args.get('x'))
        joystick.y = int(request.args.get('y'))

        pub_joyRight.publish(joystick)

        return render_template('index.html')


    if __name__ == '__main__':
        # Run Flask app
        wifi_ip = '192.168.137.91'
        app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=False)


YeetbotCamera.py
-------------------------------------

.. code-block:: python

    #!/usr/bin/env python
    """
    Node for capturing Jetbot camera
    """

    import os
    import rospy
    import cv2
    from cv2 import aruco
    import numpy as np
    from cv_bridge import CvBridge
    from sensor_msgs.msg import Image
    from std_msgs.msg import Int64
    from mas507.msg import ArucoData

    class JetbotCamera(object):
        # Set ccamera capture settings
        FRAME_RATE = 30
        VIDEO_WIDTH = 720
        VIDEO_HEIGHT = 540

        def __init__(self):        
            # Initilize GStreamer capture
            self.capture = self.get_gstreamer_capture()

            # Load camera calibration matrix from  Python calibration
            data = np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
            self.calibration_matrix = data['mtx']
            self.distortion_parameters = data['dist']

            # Load color calibration matrix
            data = np.load('%s/catkin_ws/src/mas507/data/colorCalibration.npz' % (os.path.expanduser("~")))
            self.gain_matrix = data["arr_0"] # Original load without fix
            data.close()

            # Splits up calibration into making a map, and doing the remapping in calibrate
            # member function
            # This is faster than doing cv2.undistort()
            self.map1, self.map2 = cv2.initUndistortRectifyMap(
                self.calibration_matrix,
                self.distortion_parameters,
                np.eye(3),
                self.calibration_matrix,
                (self.VIDEO_WIDTH, self.VIDEO_HEIGHT),
                cv2.CV_32FC1
            )

        def read(self):
            # Read raw image
            _, cv_raw = self.capture.read()
        
            # Perform color calibration
            image_float = np.array(cv_raw, float) * self.gain_matrix

            # Clip to keep values between 0 and 255 
            # Using [:] sets the value of object "image" without making a new object
            # i.e. just replaces in-object which does not need a return statement        
            cv_raw[:] = np.clip(image_float, 0, 255)

            return cv_raw

        def calibrate(self, image):
            """
            Perform calibration of input OpenCV image
            """

            # Make copy of input image

            # # Perform color calibration
            # image_float = np.array(image, float) * self.gain_matrix

            # # Clip to keep values between 0 and 255 
            # # Using [:] sets the value of object "image" without making a new object
            # # i.e. just replaces in-object which does not need a return statement        
            # calibrated[:] = np.clip(image_float, 0, 255)

            # Apply camera distortion calibration
            calibrated = image.copy()
            calibrated[:] = cv2.remap(calibrated, self.map1, self.map2, cv2.INTER_LINEAR)
            return calibrated

        
        # Private methods
        def get_gstreamer_capture(self):
            """
            gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
            Defaults to 1280x720 @ 60fps
            Flip the image by setting the flip_method (most common values: 0 and 2)
            display_width and display_height determine the size of the window on the screen
            """
            gstreamer_pipeline = (
                "nvarguscamerasrc ! "
                "video/x-raw(memory:NVMM), "
                "width=(int)%d, height=(int)%d, "
                "format=(string)NV12, framerate=(fraction)%d/1 ! "
                "nvvidconv flip-method=0 ! "
                "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
                "videoconvert ! "
                "video/x-raw, format=(string)BGR ! appsink drop=true sync=false"
                % (
                    self.VIDEO_WIDTH,
                    self.VIDEO_HEIGHT,
                    self.FRAME_RATE,
                    self.VIDEO_WIDTH,
                    self.VIDEO_HEIGHT,
                )
            )

            # Using default values in gstreamer_pipeline to capture video
            video_capture = cv2.VideoCapture(gstreamer_pipeline, cv2.CAP_GSTREAMER)
            return video_capture


    if __name__ == '__main__':
        # Tries to start image publisher if roscore is properly running
        try:
            # Initialize nodes
            rospy.init_node('jetbotCamera')

            # Initilize Jetbot camera instance
            camera = JetbotCamera()

            # CvBridge for converting cv2 to ROS images
            bridge = CvBridge()

            # ROS Image Publishers
            pub_raw = rospy.Publisher('image_raw', Image, queue_size=1)
            pub_calibrated = rospy.Publisher('image_calibrated', Image, queue_size=1)
            pub_markers = rospy.Publisher('image_markers', Image, queue_size=1)

            # Aruco Data Publisher
            pub_aruco = rospy.Publisher('arucoData', ArucoData, queue_size=1)

            # Init Aruco data
            aruco_x = 0
            aruco_y = 0
            aruco_z = 0
            aruco_id = 0

            # Intrinsic calibration
            intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))

            # Start Synchronous ROS node execution
            rate = rospy.Rate(10)
            while not rospy.is_shutdown():
                # Read raw image
                cv_raw = camera.read()

                # Calibrate raw image
                cv_calibrated = camera.calibrate(cv_raw)

                # Detect Aruco markers
                cv_gray = cv2.cvtColor(cv_calibrated, cv2.COLOR_BGR2GRAY)
                aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
                parameters = aruco.DetectorParameters_create()
                corners, ids, rejectedImgPoints = aruco.detectMarkers(
                    cv_gray, aruco_dict, parameters=parameters
                )

                # Find Aruco position
                aruco_markerLength = 160 # [mm]
                rvecs, tvecs, _objPoints = aruco.estimatePoseSingleMarkers(corners, aruco_markerLength, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

                if tvecs is not None:
                    aruco_x = tvecs[0,0,0]
                    aruco_y = tvecs[0,0,1] 
                    aruco_z= tvecs[0,0,2] - 60 #tuned after real world measurements
                    aruco_id = ids[0,0]
                else:
                    aruco_id = -1

                


                

                # Print Aruco marker detection onto image
                cv_markers = aruco.drawDetectedMarkers(cv_calibrated.copy(), corners, ids)
                
                # Publish images to ROS messages
                pub_raw.publish(bridge.cv2_to_imgmsg(cv_raw, "bgr8"))        
                pub_calibrated.publish(bridge.cv2_to_imgmsg(cv_calibrated, "bgr8"))
                pub_markers.publish(bridge.cv2_to_imgmsg(cv_markers, "bgr8"))

                # Publish aruco data
                pub_aruco.publish(x=aruco_x, y=aruco_y, z=aruco_z, id=aruco_id)

                # Sleep remaining time
                rate.sleep()


        except rospy.ROSInterruptException:
            pass


YeetbotController.py
-------------------------------------

.. code-block:: python

    #!/usr/bin/env python
    """
    Node for controlling Jetbot robot
    """
    import rospy
    import numpy as np
    from std_msgs.msg import Int64
    from mas507.msg import ServoSetpoints, ServoTargets
    import Adafruit_PCA9685
    import time
    import math

    # Yeetbot specific values
    servo1_threshold = [200, 450]
    servo2_threshold = [200, 500]
    servo3_threshold = [200, 480]

    servo2_range = [-20, 60] # [degrees] range of servo2

    leftWheelIdle = 338
    rightWheelIdle = 328

    # User controllable variables
    initialServos = [servo1_threshold[1], 450, servo1_threshold[0]]
    servoRate = 100 #[Hz] Update rate of the servo controller
    servoStep = 6 # Step value for incrementing the arm position of the robot


    # Miscellaneous functions
    def increment(value): # increments the first value of an array
        value[0] += 1
        return value[0]

    def stepper(currentValue, stepSize, targetValue): # steps a value towards a target with zero overshoot or undershoot
        if currentValue < (targetValue - stepSize):
            currentValue += stepSize

        elif currentValue > (targetValue + stepSize):
            currentValue -= stepSize

        else:
            currentValue = targetValue

        return currentValue

    def map(inputValue, inputMin, inputMax, outMin, outMax): # outputs a value in one range to a the equivalent in another range
            return outMin + ((outMax - outMin)/(inputMax - inputMin))*(inputValue - inputMin)


    class Yeetbot():
        def __init__(self):
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            self.servoTargets = initialServos[:]
            self.driveBias = 0 # -1 turn left, 1 turn right, 0 no bias
            self.drive = False #variable used for timed movement

            b = [0]

            # states
            self.state_stop = increment(b) # stop yeetbot
            self.state_findAruco = increment(b) # search for an aruco marker
            self.state_move2aruco = increment(b) # moves to closest aruco marker
            self.state_move2strawberry = increment(b) # moves to closest strawberry
            self.state_pickStrawberry = increment(b) # picks strawberry
            self.state_return2track = increment(b) # return to track after successfull strawberry picking
            self.state_return2aruco = increment(b) # return to previous aruco

            # initial state
            self.state = self.state_findAruco


        def moveForward(self, time2move, moveSpeed): # move forward for a certain amount of time, returns true when finished moving
            self.leftWheel = leftWheelIdle + moveSpeed
            self.rightWheel = rightWheelIdle - moveSpeed
            
            if self.drive == False:
                self.drive = True
                return False
            else:
                time.sleep(time2move)
                self.drive = False
                self.leftWheel = leftWheelIdle
                self.rightWheel = rightWheelIdle
                return True
            
        def moveBackward(self, time2move, moveSpeed): #move backward for a certain amount of time, returns true when finished moving
            self.leftWheel = leftWheelIdle - moveSpeed
            self.rightWheel = rightWheelIdle + moveSpeed
            
            if self.drive == False:
                self.drive = True
                return False
            else:
                time.sleep(time2move)
                self.drive = False
                self.leftWheel = leftWheelIdle
                self.rightWheel = rightWheelIdle
                return True
            
        def stop(self): 
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            
        def turnLeft(self, time2move, moveSpeed): 
            time.sleep(time2move)

            if self.drive == False:
                self.leftWheel = leftWheelIdle
                self.rightWheel = rightWheelIdle
                self.drive = True
                return False
            else:
                self.leftWheel = leftWheelIdle - moveSpeed -10
                self.rightWheel = rightWheelIdle - moveSpeed
                self.drive = False
                return True
            

        def turnRight(self, time2move, moveSpeed):
            time.sleep(time2move)
            
            if self.drive == False:
                self.leftWheel = leftWheelIdle
                self.rightWheel = rightWheelIdle
                self.drive = True
                return False
            else:
                self.leftWheel = leftWheelIdle + moveSpeed
                self.rightWheel = rightWheelIdle + moveSpeed + 10
                self.drive = False
                return True
            

        def followTarget(self, posOffset, moveSpeed): # follow a specified target
            self.driveBias = float(posOffset)/600
            
            if self.driveBias >= 0: # right turn bias
                min(self.driveBias, 1)
                self.leftWheel = leftWheelIdle + moveSpeed
                self.rightWheel = rightWheelIdle - moveSpeed*(1-self.driveBias)
            else:
                max(self.driveBias, -1) # left turn bias
                self.leftWheel = leftWheelIdle + moveSpeed*(1+self.driveBias)
                self.rightWheel = rightWheelIdle - moveSpeed

        def backAwayFromTarget(self, posOffset, moveSpeed): # follow a specified target
            self.driveBias = float(posOffset)/600

            if self.driveBias >= 0: # right turn bias
                min(self.driveBias, 1)
                self.leftWheel = leftWheelIdle - moveSpeed*(1-self.driveBias)
                self.rightWheel = rightWheelIdle + moveSpeed
            else:
                max(self.driveBias, -1) # left turn bias
                self.leftWheel = leftWheelIdle - moveSpeed
                self.rightWheel = rightWheelIdle + moveSpeed*(1+self.driveBias)


        def targetStrawberry(self, strawberryY, currentServos):
            yOffset = 90 # [mm] robot arm motor axel height above floor
            armLength = 150 # [mm] length of the arm
            
            # calculate strawberry height to servo motor value
            armRad = math.asin(max(min(float(strawberryY-yOffset)/armLength, 1), -1))
            armDeg = armRad*180/np.pi
            armDegAdjusted = max(min(armDeg, servo2_range[1]), servo2_range[0])  # [degrees] target angle limited to servo2 range
            print(armRad, armDeg, strawberryY, armDegAdjusted)

            self.servoTargets[1] = int(map(armDegAdjusted, servo2_range[0], servo2_range[1] ,servo2_threshold[0] ,servo2_threshold[1]))

            # moves arm to the center of the view
            self.servoTargets[0] = 405

            
            if currentServos == self.servoTargets:
                return True
            else:
                return False


        def openArm(self, currentServos):
            self.servoTargets[2] = servo3_threshold[0]

            # check if servos have reached their target values
            if currentServos == self.servoTargets:
                return True
            else:
                return False
            

        def closeArm(self, currentServos):
            self.servoTargets[2] = servo3_threshold[1]

            # check if servos have reached their target values
            if currentServos == self.servoTargets:
                return True
            else:
                return False


        def move2basket(self, currentServos): 
            self.servoTargets[0] = 205
            self.servoTargets[1] = 230

            # check if servos have reached their target values
            if currentServos == self.servoTargets:
                return True
            else:
                return False
            

        def resetArm(self, currentServos):
            self.servoTargets = initialServos[:]

            # check if servos have reached their target values
            if currentServos == self.servoTargets:
                return True
            else:
                return False
            


    class Joystick(object):
        def __init__(self):
            self.x = 0
            self.y = 0

        def callback(self, msg):
            self.x = msg.x
            self.y = msg.y


    class PickStateMachine(): # state machine used for strawberry picking
        def __init__(self):
            b = [0]

            # states
            self.saveStrawberryHeight = increment(b) # saves the strawberry y-position
            self.targetStrawberry = increment(b) # moves arm to the strawberry position
            self.envelopStrawberry = increment(b) # envelops the strawberry
            self.catchStrawberry = increment(b) # closes the hand around the strawberry
            self.backAway = increment(b) # backs away from the strawberry
            self.move2basket = increment(b) # moves the strawberry to the basket
            self.release = increment(b) # releases the strawberry into the basket
            self.reset = increment(b) # resets the arm
            self.check = increment(b) # checks if the strawberry was successfully picked

            # initial state
            self.state = self.saveStrawberryHeight

        def resetState(self):
            self.state = self.saveStrawberryHeight


    class YeetbotController(object):
        def __init__(self):
            # Adafruit PCA board instance setup
            self.adafruit = Adafruit_PCA9685.PCA9685(busnum=1)
            self.adafruit.set_pwm_freq(50)
            
        def setPWM(self, u):
            """
            Set RC parameter for wheel motors driven by Adafruit
            PCA9685 with 12 bit resolution (2^12 = 4096) for each servo
            On 50 Hz rate (20 ms period), 409 ticks=2ms, 204ticks=1ms

            Parameters
            ----------
            u[i]:
                Uptime of RC signal in ticks
                Between [204 and 408]
                where, theoretically, 306 is stand-still, 204 is maximum reverse
                and 408 is maximum forward.

                i=0: right motor
                i=1: left motor
                i=2: servo 1
                i=3: servo 2
                i=4: servo 3
            """

            for i in range(0, 5):
                # Limit input to u in [204, 408]
                u[i] = max(204, u[i])

                if i < 2:
                    u[i] = min(408, u[i])

                else:
                    u[i] = min(600, u[i])
                
                # Set PWM
                self.adafruit.set_pwm(i, 0, u[i])


    def servoCallback(msg): # read data from ServoSetpoints message
        global servos
        
        servos = [msg.servo1, msg.servo2, msg.servo3]

    def servoTargetsCallback(msg): # read data from ServoTargets message
        global rightWheel
        global leftWheel
        global servoTargets
        
        rightWheel = msg.rightWheel
        leftWheel = msg.leftWheel
        servoTargets = [msg.servo1_target, msg.servo2_target, msg.servo3_target]



    if __name__ == '__main__':
        try:
            # Init ROS node
            rospy.init_node('yeetbotController', anonymous=True)

            # Jetbot controller
            yeetbotController = YeetbotController()

            # Initialize servo setpoint values
            servoSetpoints = ServoSetpoints()
            servos = initialServos[:]
            servoTargets = initialServos[:]
            rightWheel = rightWheelIdle
            leftWheel = leftWheelIdle


            # Publisher
            pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)

            # Subscriber
            rospy.Subscriber("servoTargets", ServoTargets, servoTargetsCallback)
            rospy.Subscriber("servoSetpoints", ServoSetpoints, servoCallback)
            
            # Start Synchronous ROS node execution
            rate = rospy.Rate(servoRate)

            #t = 0

            while not rospy.is_shutdown():
                
                # coerce servo setpoint targets to their respective threshold values
                servoTargets[0] = max(min(servoTargets[0], servo1_threshold[1]), servo1_threshold[0])
                servoTargets[1] = max(min(servoTargets[1], servo2_threshold[1]), servo2_threshold[0])
                servoTargets[2] = max(min(servoTargets[2], servo3_threshold[1]), servo3_threshold[0])

                servoSetpoints.servo1 = stepper(servos[0], servoStep, servoTargets[0])
                servoSetpoints.servo2 = stepper(servos[1], servoStep, servoTargets[1])
                
                servoSetpoints.servo3 = servoTargets[2]

                
                # publishes servo setpoint values
                pub_servoSetpoints.publish(servoSetpoints)

                
                # Set Servo PWMs
                yeetbotController.setPWM([rightWheel, leftWheel, servos[0], servos[1], servos[2]])

                #print(t)
                #t += float(1)/servoRate

                # Sleep remaining time
                rate.sleep()


        except rospy.ROSInterruptException:
            pass
