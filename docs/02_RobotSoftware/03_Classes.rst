Classes
============================

Yeetbot
-------------------------------------

The "Yeetbot" class documented in this section contains several values and functions related to the operation of the Yeetbot along with its state machine.


**State Machine and Operational Values**

The state machine with all of its different states along with several key operational values are defined in the initializer
function of the class and is shown in its entirety below.

The values "leftWheel" and "rightWheel" are the target values for
the continuous servo motors controlling the wheels of the Yeetbot. They are initially set to their idle values, which are
values that lead to no movement of the motors.

The "servoTargets" list contains all three target values of the three positional
rotation servo motors operating the arm of the Yeetbot. They are initially set to refer to the arm being completely upright and
rotated away from the camera, along with the gripper being in its open position.

The "driveBias" value and "drive" value are explained further in the "Functions" section below.

The values underneath the "states" comment refer to the different states of the Yeetbot. The list "b" is used along with the 
"increment" function, explained in :numref:`Section %s <Increment Value>`, in order to define the different states to unique values.

.. code-block:: python

    def __init__(self):
        self.leftWheel = leftWheelIdle
        self.rightWheel = rightWheelIdle
        self.servoTargets = initialServos[:]
        self.driveBias = 0 # -1 turn left, 1 turn right, 0 no bias
        self.drive = False #variable used for timed movement

        b = [0]

        # states
        self.state_stop = increment(b) # stop yeetbot
        self.state_findAruco = increment(b) # search for an aruco marker
        self.state_move2aruco = increment(b) # moves to closest aruco marker
        self.state_move2strawberry = increment(b) # moves to closest strawberry
        self.state_pickStrawberry = increment(b) # picks strawberry
        self.state_return2track = increment(b) # return to track after successfull strawberry picking
        self.state_return2aruco = increment(b) # return to previous aruco

        # initial state
        self.state = self.state_findAruco


**Move Functions**

The functions "moveForward" and "moveBackward" shown below moves the Yeetbot respectively forward and backward with a specified move
speed for a specified time. The move speed is not an actual unit of velocity, but rather the value used to offset the idle duty cycle value of
the continuous servo motors used for the wheels. Both functions return True when they are finished moving.


.. code-block:: python

    def moveForward(self, time2move, moveSpeed):
        self.leftWheel = leftWheelIdle + moveSpeed
        self.rightWheel = rightWheelIdle - moveSpeed
        
        if self.drive == False:
            self.drive = True
            return False
        else:
            time.sleep(time2move)
            self.drive = False
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            return True
    
    def moveBackward(self, time2move, moveSpeed):
        self.leftWheel = leftWheelIdle - moveSpeed
        self.rightWheel = rightWheelIdle + moveSpeed
        
        if self.drive == False:
            self.drive = True
            return False
        else:
            time.sleep(time2move)
            self.drive = False
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            return True


**Stop Function**

The function "stop" shown below resets the continuous servo motors used for the wheels of the Yeetbot to their initial idle positions.

.. code-block:: python

    def stop(self): 
        self.leftWheel = leftWheelIdle
        self.rightWheel = rightWheelIdle


**Turn Functions**

The functions "turnLeft" and "turnRight" shown below rotates the Yeetbot respectively anticlockwise and clockwise with a specified move
speed for a specified time. Both functions return True when they are finished moving.

.. code-block:: python

    def turnLeft(self, time2move, moveSpeed): 
        time.sleep(time2move)

        if self.drive == False:
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            self.drive = True
            return False
        else:
            self.leftWheel = leftWheelIdle - moveSpeed -10
            self.rightWheel = rightWheelIdle - moveSpeed
            self.drive = False
            return True
        

    def turnRight(self, time2move, moveSpeed):
        time.sleep(time2move)
        
        if self.drive == False:
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            self.drive = True
            return False
        else:
            self.leftWheel = leftWheelIdle + moveSpeed
            self.rightWheel = rightWheelIdle + moveSpeed + 10
            self.drive = False
            return True


**Follow Function**

The function "followTarget" shown below continuously aligns the Yeetbot's trajectory based on a positional offset of a target, "posOffset", while
moving towards the target with a specified move speed. The positional offset relates to the offset of the target from the center x-axis of the Yeetbot.

.. code-block:: python

    def followTarget(self, posOffset, moveSpeed):
        self.driveBias = float(posOffset)/600
        
        if self.driveBias >= 0: # right turn bias
            min(self.driveBias, 1)
            self.leftWheel = leftWheelIdle + moveSpeed
            self.rightWheel = rightWheelIdle - moveSpeed*(1-self.driveBias)
        else:
            max(self.driveBias, -1) # left turn bias
            self.leftWheel = leftWheelIdle + moveSpeed*(1+self.driveBias)
            self.rightWheel = rightWheelIdle - moveSpeed

**Back Away From Function**

The function "backAwayFromTarget" shown below continuously aligns the Yeetbot's trajectory based on a positional offset of a target, "posOffset", while
moving away from the target with a specified move speed. The positional offset relates to the offset of the target from the center x-axis of the Yeetbot.

.. code-block:: python

    def backAwayFromTarget(self, posOffset, moveSpeed): # follow a specified target
        self.driveBias = float(posOffset)/600

        if self.driveBias >= 0: # right turn bias
            min(self.driveBias, 1)
            self.leftWheel = leftWheelIdle - moveSpeed*(1-self.driveBias)
            self.rightWheel = rightWheelIdle + moveSpeed
        else:
            max(self.driveBias, -1) # left turn bias
            self.leftWheel = leftWheelIdle - moveSpeed
            self.rightWheel = rightWheelIdle + moveSpeed*(1+self.driveBias)


**Target Strawberry Function**

The function "targetStrawberry" shown below moves the gripper of the arm to the center of the x axis of the Yeetbot while simultaneously
moving it to the height of the strawberry. The function calculates the necessary servo position to match the strawberry height as described
in :numref:`Section %s <Robot Arm Kinematics>`. The function returns True when all positional servo motors are set to their specified target values.


.. code-block:: python

    def targetStrawberry(self, strawberryY, currentServos):
        yOffset = 90 # [mm] robot arm motor axel height above floor
        armLength = 150 # [mm] length of the arm
        
        # calculate strawberry height to servo motor value
        armRad = math.asin(max(min(float(strawberryY-yOffset)/armLength, 1), -1))
        armDeg = armRad*180/np.pi
        armDegAdjusted = max(min(armDeg, servo2_range[1]), servo2_range[0])  # [degrees] target angle limited to servo2 range
        print(armRad, armDeg, strawberryY, armDegAdjusted)

        self.servoTargets[1] = int(map(armDegAdjusted, servo2_range[0], servo2_range[1] ,servo2_threshold[0] ,servo2_threshold[1]))

         # moves arm to the center of the view
        self.servoTargets[0] = 405

        
        if currentServos == self.servoTargets:
            return True
        else:
            return False


**Open and Close Arm Function**

The functions "openArm" and "closeArm" shown below respectively open and close the gripper of the arm.
Both functions return True when all positional servo motors are set to their specified target values.

.. code-block:: python

    def openArm(self, currentServos):
        self.servoTargets[2] = servo3_threshold[0]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False
        

    def closeArm(self, currentServos):
        self.servoTargets[2] = servo3_threshold[1]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False


**Move to Basket Function**

The function "move2basket" shown below moves the arm of the Yeetbot so that the gripper is located above the basket used for storing picked strawberries.
The function returns true when all positional servo motors are set to their specified target values.

.. code-block:: python

    def move2basket(self, currentServos): 
        self.servoTargets[0] = 205
        self.servoTargets[1] = 230

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False


**Reset Arm Function**

The function "resetArm" resets the arm and gripper back to their initial positions.
The function returns true when all positional servo motors are set to their specified target values.

.. code-block:: python

    def resetArm(self, currentServos):
        self.servoTargets = initialServos[:]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False



Strawberry Picking State Machine
-------------------------------------

The "PickStateMachine" class documented in this section handles the state machine of the strawberry picking operation of the state machine.

**State Machine**

The state machine with all of its different states is defined in the initializer function of the class and is shown in its entirety below.

.. code-block:: python

    def __init__(self):
        b = [0]

        # states
        self.saveStrawberryHeight = increment(b) # saves the strawberry y-position
        self.targetStrawberry = increment(b) # moves arm to the strawberry position
        self.envelopStrawberry = increment(b) # envelops the strawberry
        self.catchStrawberry = increment(b) # closes the hand around the strawberry
        self.backAway = increment(b) # backs away from the strawberry
        self.move2basket = increment(b) # moves the strawberry to the basket
        self.release = increment(b) # releases the strawberry into the basket
        self.reset = increment(b) # resets the arm
        self.check = increment(b) # checks if the strawberry was successfully picked

        # initial state
        self.state = self.saveStrawberryHeight


**Reset Function**

The function "resetState" shown below resets the state of the state machine to its initial state.

.. code-block:: python

    def resetState(self):
        self.state = self.saveStrawberryHeight


Servo Controller
-------------------------------------

The class servo controller class, known in the code as "YeetbotController", handles writing to the servo controller board, Adafruit PCA9685, used in the Yeetbot.

**Initializer Function**

The initializer function shown below assigns the busnumber of the servo controller along with the PWM frequency used for the servo motors.

.. code-block:: python

    def __init__(self):
        self.adafruit = Adafruit_PCA9685.PCA9685(busnum=1)
        self.adafruit.set_pwm_freq(50)

**Update Servo Controller Function**

The function "setPWM" shown below takes a list containing all specified target servo motor duty cycle values and limits them to their specific thresholds (204 to 408 for the continuous
rotating servo motors, and 204 to 600 for the positional servo motors) and then writes them to the servo controller board.

.. code-block:: python

    def setPWM(self, u):
        """
        Set RC parameter for wheel motors driven by Adafruit
        PCA9685 with 12 bit resolution (2^12 = 4096) for each servo
        On 50 Hz rate (20 ms period), 409 ticks=2ms, 204ticks=1ms

        Parameters
        ----------
        u[i]:
            Uptime of RC signal in ticks
            Between [204 and 408]
            where, theoretically, 306 is stand-still, 204 is maximum reverse
            and 408 is maximum forward.

            i=0: right motor
            i=1: left motor
            i=2: servo 1
            i=3: servo 2
            i=4: servo 3
        """

        for i in range(0, 5):
            # Limit input to u in [204, 408]
            u[i] = max(204, u[i])

            if i < 2:
                u[i] = min(408, u[i])

            else:
                u[i] = min(600, u[i])
            
            # Set PWM
            self.adafruit.set_pwm(i, 0, u[i])


Strawberry Detector
-------------------------------------

The strawbery detector class, "StrawberryDetector", was supplied by the MAS507 course and can be seen in its entirety in :numref:`Chapter %s <Appendix C>`.
This class has been modified to return -1 when no strawberries are detected and is modified so that the y-position of the strawberry refers to its height
above ground at picking distance instead of its center offset.
