Miscellaneous Functions
============================

The functions shown in this section are some general functions made for use in the Yeetbot's robot software and are not part of a public library.

Increment Value
-------------------------------------
The "increment" function shown below increments the first value in a given list object by one. This function takes advantage of the
fact that list objects in python are "mutable", which in effect means that if the list is passed into a function and then changed
it will also be changed in the script that called it. This differs from an "immutable" object, such as an int, which would only
be changed locally within the function.

.. code-block:: python

    def increment(value):
    value[0] += 1
    return value[0]


Step Value
-------------------------------------
The "stepper" function shown below takes a value, "currentValue", and increments it with a certain step size towards a target value.
If the input value would exceed the target value upon being incremented, it is instead set equal to the target value.

.. code-block:: python

    def stepper(currentValue, stepSize, targetValue):
    if currentValue < (targetValue - stepSize):
        currentValue += stepSize

    elif currentValue > (targetValue + stepSize):
        currentValue -= stepSize

    else:
        currentValue = targetValue

    return currentValue


Map Value
-------------------------------------
The "map" function shown below takes an input value with a specific min/max range and converts it to an equivalent value in a
different specified range.

.. code-block:: python

    def map(inputValue, inputMin, inputMax, outMin, outMax):
        return outMin + ((outMax - outMin)/(inputMax - inputMin))*(inputValue - inputMin)



