ROS Messages
============================
The messages explained in this section are used for communication between the various nodes explained in the following sections.


Servo Setpoints
-------------------------------------
The ROS message "ServoSetpoints" shown below is used to communicate the current state of the three positional rotation servo motors used for the arm
operation of the Yeetbot.

.. code-block:: python

    int64 servo1
    int64 servo2
    int64 servo3

The callback function used when subscribing to this message is shown below. The callback takes all servo values from the message and writes them to a
global list to make them available to use within the relevant ROS nodes.

.. code-block:: python

    def servoCallback(msg):
    global servos
    
    servos = [msg.servo1, msg.servo2, msg.servo3]


Servo Targets
-------------------------------------

The ROS message "ServoTargets" shown below is used to communicate the target duty cycle values of all the servo motors of the Yeetbot.

.. code-block:: python

    int64 rightWheel
    int64 leftWheel
    int64 servo1_target
    int64 servo2_target
    int64 servo3_target

The callback function used when subscribing to this message is shown below. The callback takes the target servo values of all the servo motors
of the Yeetbot and makes them available for use within the relevant ROS nodes. The target values of the positional rotation servo motors are
made available in a global list, while the target values of the continuous rotation servo motors are written to individual global int objects.


.. code-block:: python
    def servoTargetsCallback(msg): 
    global rightWheel
    global leftWheel
    global servoTargets
    
    rightWheel = msg.rightWheel
    leftWheel = msg.leftWheel
    servoTargets = [msg.servo1_target, msg.servo2_target, msg.servo3_target]


Aruco Data
-------------------------------------
The ROS message "ArucoData" shown below is used to communicate the position and identification value of a detected ArUco marker.


.. code-block:: python

    int64 x
    int64 y
    int64 z
    int64 id

The callback function used when subscribing to this message is shown below. The callback takes all the positional data along with the identification value
and makes them available for use within the relevant ROS nodes through individual global int objects.
.. code-block:: python
    def arucoCallback(msg):
    global aruco_x
    global aruco_y
    global aruco_z
    global aruco_id

    aruco_x = msg.x
    aruco_y = msg.y
    aruco_z = msg.z
    aruco_id = msg.id




ROS Nodes
============================
This section contains documentation of all simultaneously running ROS nodes of the Yeetbot.

Camera
-------------------------------------
The camera node used in the Yeetbot is mostly identical to the "JetbotCamera.py" script supplied by the MAS507 course. The script handles the reading and processing
of the camera data, along with streaming the resulting data to the localhost of the computer. The camera node is configured to detect ArUco markers and display them in a
unique video stream. The positional and identification from the detected ArUco markers are retrieved using the code shown below, which is located within the synchronous ROS node
of the camera node. The ArUco markers are published to the "ArucoData" ROS message using the publisher "pub_aruco" (not shown below). The ArUco identification value, "aruco_id", is
set to -1 if no ArUco marker is detected.

.. code-block:: python

    aruco_markerLength = 160 # [mm]
        rvecs, tvecs, _objPoints = aruco.estimatePoseSingleMarkers(corners, aruco_markerLength, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        if tvecs is not None:
            aruco_x = tvecs[0,0,0]
            aruco_y = tvecs[0,0,1] 
            aruco_z= tvecs[0,0,2] - 60 #tuned after real world measurements
            aruco_id = ids[0,0]
        else:
            aruco_id = -1

    # Publish aruco data
    pub_aruco.publish(x=aruco_x, y=aruco_y, z=aruco_z, id=aruco_id)



Servo Controller
-------------------------------------

The servo controller node shown below controls the Yeetbot's three positional rotation servo motors used for the arm along with the two continuous
servo motors used for the wheels. The control of the servo motors are handled independently in this node instead of the main controller, see
:numref:`Section %s <Main Controller>` below, to enable the servo motors to update at a higher frequency than the main operation script of the Yeetbot itself.
The rate of this node is set to 100 hertz, as a rate higher than this lead to the node running at an unstable rate. 

The servo controller node subscribes to both the "ServoSetpoints" and "ServoTargets" ROS messages. It takes the duty cycle values of the two position rotation servo motors of the arm
used for translation and gradually increments them using the "stepper" function, explained in :numref:`Section %s <Step Value>`, towards the their individual target values. This is done
to avoid the arm yerking into position and causing unnecessary strain on the servo motors. The duty cycle of the servo motor used for the gripper is instead instantaneously set to its
target value to avoid a strawberry potentially falling out of the gripper before it has fully closed. As smooth movement is achieved regardless of the rate of change, the duty cycles
of the two continuous servo motors used for the wheels are also set to their target value instantaneously. The servo values of the three positional rotation servo motors are then published
using the "pub_servoSetpoint" publisher.

.. code-block:: python

    if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('yeetbotController', anonymous=True)

        # Jetbot controller
        yeetbotController = YeetbotController()

        # Initialize servo setpoint values
        servoSetpoints = ServoSetpoints()
        servos = initialServos[:]
        servoTargets = initialServos[:]
        rightWheel = rightWheelIdle
        leftWheel = leftWheelIdle


        # Publisher
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)

        # Subscriber
        rospy.Subscriber("servoTargets", ServoTargets, servoTargetsCallback)
        rospy.Subscriber("servoSetpoints", ServoSetpoints, servoCallback)
        
        # Start Synchronous ROS node execution
        rate = rospy.Rate(servoRate)

        #t = 0

        while not rospy.is_shutdown():
            
            # coerce servo setpoint targets to their respective threshold values
            servoTargets[0] = max(min(servoTargets[0], servo1_threshold[1]), servo1_threshold[0])
            servoTargets[1] = max(min(servoTargets[1], servo2_threshold[1]), servo2_threshold[0])
            servoTargets[2] = max(min(servoTargets[2], servo3_threshold[1]), servo3_threshold[0])

            servoSetpoints.servo1 = stepper(servos[0], servoStep, servoTargets[0])
            servoSetpoints.servo2 = stepper(servos[1], servoStep, servoTargets[1])
           
            servoSetpoints.servo3 = servoTargets[2]

            
            # publishes servo setpoint values
            pub_servoSetpoints.publish(servoSetpoints)

            
            # Set Servo PWMs
            yeetbotController.setPWM([rightWheel, leftWheel, servos[0], servos[1], servos[2]])

            #print(t)
            #t += float(1)/servoRate

            # Sleep remaining time
            rate.sleep()


    except rospy.ROSInterruptException:
        pass



Main Controller
-------------------------------------

The main controller node documented in this section handles the actual operation of the Yeetbot. The main controller
is, in simple terms, the implementation of the flowchart shown in :numref:`Figure %s <flowchart>`.

.. figure:: Figures/FlowchartTransparent.png
    :name: flowchart
    :scale: 100%
    :align: center
    :alt: image 1   

    The state flowchart describing the main operation of the Yeetbot.

The main controller node runs at a rate of 10 hertz and subscribes to the "arucoData" and "servoSetpoints" messages. Additionally it subscribes to a calibrated image message
used for returning the position of a detected strawberry using the "StrawberryDetector" code, appended in :numref:`Chapter %s <Appendix C>`, supplied by the MAS507 course.

**Status Messages**

The main controller prints status messages to the terminal when a new status message is queued using the code below.

.. code-block:: python

    if statusMessage != statusMessagePrevious: #only print when a new message is queued
        print(statusMessage)
        statusMessagePrevious = statusMessage


**State: Find ArUco**

This state is the initial state of the Yeetbot. The state checks if a new unique ArUco marker is detected and then sets the state of the Yeetbot to the "Move to Aruco" state.
It does so by stepwise rotating the Yeetbot clockwise for a set amount until an ArUco is detected. The Yeetbot does not rotate continuously as it is not able to reliably detect
an ArUco unless the camera view is stable. The state contains a buffer of two previously detected ArUco identification values to avoid re-detection of a previous ArUco. The state
also counts the total numbert of ArUco markers detected. If the Yeetbot has already detected three unique ArUco markers it will set the state of the Yeetbot to the "Stop" state.

.. code-block:: python

    if yeetbot.state == yeetbot.state_findAruco:
        statusMessage = 'Searching for ArUco...'
        
        # checks if a new aruco is found
        if yeetbot.turnRight(turnTime, turnSpeed) == True and aruco_id != -1 and aruco_id != aruco_id_previous[0] and aruco_id != aruco_id_previous[1]: 
            statusMessage = 'ArUco found!'
            aruco_id_previous[0] = aruco_id_previous[1]
            aruco_id_previous[1] = aruco_id
            arucoCounter = arucoCounter + 1
            yeetbot.state = yeetbot.state_move2aruco
        
        # stops robot if all aruco markers have been found
        elif arucoCounter >= 3:
            yeetbot.state = yeetbot.state_stop


**State: Move to ArUco**

This state makes the Yeetbot move towards a located ArUco marker. If a strawberry is detected within a set minimum range the state will be changed to the "Move to Strawberry" state.
If the Yeetbot has reached a minimum distance to the ArUco marker the state will be changed to the "Find Aruco" state. If the Yeetbot loses detection of the ArUco marker the state will
be changed to the "Return to ArUco" state.

.. code-block:: python

    elif yeetbot.state == yeetbot.state_move2aruco:
        statusMessage = 'Moving to ArUco...'
        yeetbot.followTarget(aruco_x, moveSpeedAruco)
        distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

            # checks if yeetbot is within minimum distance of a strawberry
        if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance and arucoCounter != 2:
            statusMessage = 'Strawberry found!'
            yeetbot.state = yeetbot.state_move2strawberry

        # checks if yeetbot has reached stop-distance from aruco marker
        elif aruco_z <= arucoStopDistance and aruco_id != -1: 
            statusMessage = 'ArUco distance limit reached.'
            yeetbot.stop()
            yeetbot.state = yeetbot.state_findAruco

        # returns to aruco if aruco id is lost
        elif aruco_id == -1:
            statusMessage = 'ArUco lost!'
            yeetbot.state = yeetbot.state_return2aruco


**State: Move to Strawberry**

This state makes the Yeetbot move towards or back away from a located strawberry until a specified distance away from the strawberry has been reached and it is coincident
with the center x-axis of the Yeetbot. The Yeetbot's state will then be changed to the "Pick Strawberry" state. If the Yeetbot loses detection of the strawberry the state
changed to the "Return to Track" state.


.. code-block:: python

    elif yeetbot.state == yeetbot.state_move2strawberry:

        if strawberryDetector.exist == 1:
            statusMessage = 'Moving towards the strawberry...'
            distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

            # ensures that the yeetbot is neither too far away nor too close to the strawberry
            if distance2strawberry > strawberryPickDistance:
                yeetbot.followTarget((strawberryDetector.x)*2, moveSpeedStrawberry)

            else:
                yeetbot.backAwayFromTarget((strawberryDetector.x)*2, (moveSpeedStrawberry+5))
            
                # check if strawberry is in the center of the camera-view and within acceptable range
            if abs(strawberryDetector.x) <= 35 and (strawberryPickDistance - 6) <= distance2strawberry <= (strawberryPickDistance+6):
                statusMessage = 'Strawberry reached.'
                yeetbot.stop()
                yeetbot.state = yeetbot.state_pickStrawberry
                
        else:
            statusMessage = 'Strawberry lost!'
            yeetbot.state = yeetbot.state_return2track

**State: Pick Strawberry**

This state handles the strawberry picking operation of the Yeetbot. This state has a sub-state handled by the "pickStateMachine" class shown
in :numref:`Section %s <Strawberry Picking State Machine>`. As the strawberry will be obscured from the camera view during picking, the height
of the strawberry is saved at the start of the state. The arm is then moved so that the gripper is at the same height of the strawberry. The Yeetbot
then moves slightly forward until the gripper completely envelops the strawberry. The "Move to Strawberry" state is designed to position the Yeetbot
so that moving the arm will not accidentally hit the strawberry nor the plant in order to prevent either the strawberry falling off prematurely or the plant itself
being knocked over. The gripper is then closed and the Yeetbot moves backwards away from the plant in order to release the strawberry from its stem. The gripper
is then moved to the basket and the strawberry released. The arm is finally reset to its original position before checking if the strawberry was successfully picked
or not. If no strawberry is detected within range the Yeetbot's state is changed to "Return to ArUco", otherwise it will change the state back to "Move to Strawberry"
in order to reattempt the picking procedure. In either case, the picking sub-state will be reset to its initial state.

Note: If a second strawberry is located close to the picked strawberry the Yeetbot will not reattempt picking the previous strawberry's position. It will instead pick
the newly detected adjacent strawberry.


.. code-block:: python

    elif yeetbot.state == yeetbot.state_pickStrawberry: 
                
        # saves the detected y value of the strawberry as it will be obstructed from view when picking
        if pickStateMachine.state == pickStateMachine.saveStrawberryHeight and strawberryDetector.exist == 1:
            savedStrawberryY = strawberryDetector.y
            pickStateMachine.state = pickStateMachine.targetStrawberry


        # moves arm to the target strawberry
        elif pickStateMachine.state == pickStateMachine.targetStrawberry:
            statusMessage = 'Targeting strawberry...'

            if yeetbot.targetStrawberry(savedStrawberryY, servos) == True:
                pickStateMachine.state = pickStateMachine.envelopStrawberry

        # moves forward towards the strawberry
        elif pickStateMachine.state == pickStateMachine.envelopStrawberry:
            statusMessage = 'Extracting strawberry...'

            if yeetbot.moveForward(1.3, 6) == True:
                pickStateMachine.state = pickStateMachine.catchStrawberry

        # closes hand around the strawberry
        elif pickStateMachine.state == pickStateMachine.catchStrawberry:

            if yeetbot.closeArm(servos) == True:
                pickStateMachine.state = pickStateMachine.backAway

        # backs away from the strawberry
        elif pickStateMachine.state == pickStateMachine.backAway:

            if yeetbot.moveBackward(1.1, 30) == True:
                pickStateMachine.state = pickStateMachine.move2basket
            
        # moves the strawberry to the basket
        elif pickStateMachine.state == pickStateMachine.move2basket:
            statusMessage = 'Moving strawberry to basket...'

            if yeetbot.move2basket(servos) == True:
                pickStateMachine.state = pickStateMachine.release

        # releases the strawberry
        elif pickStateMachine.state == pickStateMachine.release:
            
            if yeetbot.openArm(servos) == True:
                pickStateMachine.state = pickStateMachine.reset
            

        # resets the arm
        elif pickStateMachine.state == pickStateMachine.reset:
            statusMessage = 'Resetting arm...'

            if yeetbot.resetArm(servos) == True:
                statusMessage = 'Arm successfully reset.'
                pickStateMachine.state = pickStateMachine.check
        
        # checks if the strawberry was picked successfully
        elif pickStateMachine.state == pickStateMachine.check:
            
            if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                statusMessage = 'Strawberry picking failed or adjacent strawberry located.'
                pickStateMachine.resetState()
                yeetbot.state = yeetbot.state_move2strawberry

            else:
                statusMessage = 'Strawberry picked successfully!'
                pickStateMachine.resetState()
                yeetbot.state = yeetbot.state_return2aruco


**State: Return to Track**

This state makes the Yeetbot reverse for a short distance to attempt to realign it to the track. This is done as a
safety measure to provide the Yeetbot with plenty of distance from the strawberry plants so that it can safely guide to the
previous ArUco marker or begin picking the next strawberry without getting stuck in or knocking over nearby plants. Upon successfully reversing
the specified distance the state will be changed to "Return to ArUco" unless a strawberry is withing picking range upon which it will be changed
to the "Move to Strawberry" state instead.

.. code-block:: python

    elif yeetbot.state == yeetbot.state_return2track:
        statusMessage = 'Returning to track...'

        if yeetbot.moveBackward(1, 15) == True:

            if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                yeetbot.state = yeetbot.state_move2strawberry

            else:
                yeetbot.state = yeetbot.state_return2aruco


**State: Return to ArUco**

This state attempts to localize and begin following the previously followed ArUco marker. It does so by rotating anti-clockwise for a set
amount until the previous ArUco identifcation value is detected. The Yeetbot does not rotate continuously as it is not able to reliably detect
an ArUco unless the camera view is stable.

.. code-block:: python

    elif yeetbot.state == yeetbot.state_return2aruco:
        statusMessage = 'Searching for previous ArUco...'

        if yeetbot.turnLeft(turnTime, turnSpeed) == True and aruco_id == aruco_id_previous[1]: # checks if the previous aruco is detected
            statusMessage = 'Previous ArUco found!'
            yeetbot.state = yeetbot.state_move2aruco 


**State: Stop**

This state stops the Yeetbot and concludes the operation.

.. code-block:: python

    elif yeetbot.state == yeetbot.state_stop:
        statusMessage = 'Operation finished.'
        yeetbot.stop()


**Updating Servo Targets**

The "servoTargets" message is updated using the code shown below.

.. code-block:: python

    servoTargets.leftWheel  = yeetbot.leftWheel
        servoTargets.rightWheel = yeetbot.rightWheel

        # overwrites wheel servo values if joystick control is enabled
        if joystickControl == True:
            servoTargets.leftWheel  = leftWheelIdle - rightJoystick.y/2 + leftJoystick.x/7
            servoTargets.rightWheel = rightWheelIdle + rightJoystick.y/2 + leftJoystick.x/7
        

        servoTargets.servo1_target = yeetbot.servoTargets[0]
        servoTargets.servo2_target = yeetbot.servoTargets[1]
        servoTargets.servo3_target = yeetbot.servoTargets[2]
        
        # publishes servo setpoint values
        pub_servoTargets.publish(servoTargets)