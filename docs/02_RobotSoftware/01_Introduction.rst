Introduction
============================

This chapter serves as a documentation of the robot software of the "Yeetbot". The name "Yeetbot" was created
as a play-on-words on the "Jetbot" name of the base robot. This nickname is used throughout the code of the
robot software and will be used in this chapter to refer to the robot developed in this project.

The robot software code is documented in the following sections broken down to its base components.
Namely, its functions, classes, and ROS nodes.

Apart from a few exceptions, the code that were made commonly available by the MAS507 course will not be be explained in
detail. As such, this documentation comprises solely of the code made specifically for the Yeetbot.
All the python code used in this project can be seen in its entirety in :numref:`Chapter %s <Appendix C>`.

