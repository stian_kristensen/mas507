#!/usr/bin/env python
"""
Main Control Node
"""
import rospy
import numpy as np
import os
from mas507.msg import ServoTargets, WebJoystick, ArucoData, ServoSetpoints
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector
from YeetbotController import Yeetbot, Joystick, PickStateMachine, servo1_threshold, servo2_threshold, servo3_threshold, servo2_range, leftWheelIdle, rightWheelIdle, initialServos
from math import sqrt
import time


def arucoCallback(msg): # read aruco data from messages
    global aruco_x
    global aruco_y
    global aruco_z
    global aruco_id

    aruco_x = msg.x
    aruco_y = msg.y
    aruco_z = msg.z
    aruco_id = msg.id

def servoCallback(msg): # read data from ServoSetpoints message
    global servos
    
    servos = [msg.servo1, msg.servo2, msg.servo3]

if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

        # Init web joysticks
        leftJoystick = Joystick()
        rightJoystick = Joystick()

        # Init Yeetbot
        servoTargets = ServoTargets()
        yeetbot = Yeetbot()
        servos = initialServos[:]
        pickStateMachine = PickStateMachine()
        yeetbot.moveSpeed = 10 # move speed of yeetbot
        

        # Init Aruco markers coordinates
        aruco_x = 9999
        aruco_y = 9999
        aruco_z = 9999
        aruco_id = -1

        # Init various variables for control loop
        aruco_id_previous = [10, 10] #storage for previous aruco markers, important that the initial values differs from any available ID
        statusMessage = 'Initializing Yeetbot...'
        statusMessagePrevious = ' '
        arucoCounter = 0 # number of aruco markers found

        # User controllable variables
        joystickControl = False
        arucoStopDistance = 240 # [mm] used to stop the yeetbot when it gets within a certain range of an aruco marker
        strawberryPickDistance = 220 # [mm] minimum threshold to start strawberry picking sequence
        strawberryMinDistance = strawberryPickDistance + 30 # [mm] minimum distance to start moving towards a strawberry
        turnTime = 1.2 # [s] time to turn when searching
        turnSpeed = 6 # velocity value used for turning
        moveSpeedAruco = 12 # velocity value used when following an aruco marker
        moveSpeedStrawberry = 10 # velocity value used when following a strawberry
        
        
        # Publishers
        pub_servoTargets = rospy.Publisher('servoTargets', ServoTargets, queue_size=1)
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)

        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])
            


        # Subscribers
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)
        sub_leftJoystick = rospy.Subscriber('webJoystickLeft', WebJoystick, leftJoystick.callback)
        sub_rightJoystick = rospy.Subscriber('webJoystickRight', WebJoystick, rightJoystick.callback)
        sub_arucoData = rospy.Subscriber('arucoData', ArucoData, arucoCallback)
        sub_servoSetpoints = rospy.Subscriber('servoSetpoints', ServoSetpoints, servoCallback)
        
        
        # Start Synchronous ROS node execution
        rate = rospy.Rate(10)
        
        #yeetbot.state = yeetbot.state_test
        

        # Control Loop
        while not rospy.is_shutdown():

            # Status print
            if statusMessage != statusMessagePrevious: #only print when a new message is queued
                print(statusMessage)
                statusMessagePrevious = statusMessage
            
               
            # find aruco
            if yeetbot.state == yeetbot.state_findAruco:
                statusMessage = 'Searching for ArUco...'
                
                # checks if a new aruco is found
                if yeetbot.turnRight(turnTime, turnSpeed) == True and aruco_id != -1 and aruco_id != aruco_id_previous[0] and aruco_id != aruco_id_previous[1]: 
                    statusMessage = 'ArUco found!'
                    aruco_id_previous[0] = aruco_id_previous[1]
                    aruco_id_previous[1] = aruco_id
                    arucoCounter = arucoCounter + 1
                    yeetbot.state = yeetbot.state_move2aruco
                
                # stops robot if all aruco markers have been found
                elif arucoCounter >= 3:
                    yeetbot.state = yeetbot.state_stop
                
            
            # move to aruco
            elif yeetbot.state == yeetbot.state_move2aruco:
                statusMessage = 'Moving to ArUco...'
                yeetbot.followTarget(aruco_x, moveSpeedAruco)
                distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

                 # checks if yeetbot is within minimum distance of a strawberry
                if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance and arucoCounter != 2:
                    statusMessage = 'Strawberry found!'
                    yeetbot.state = yeetbot.state_move2strawberry

                # checks if yeetbot has reached stop-distance from aruco marker
                elif aruco_z <= arucoStopDistance and aruco_id != -1: 
                    statusMessage = 'ArUco distance limit reached.'
                    yeetbot.stop()
                    yeetbot.state = yeetbot.state_findAruco

                # returns to aruco if aruco id is lost
                elif aruco_id == -1:
                    statusMessage = 'ArUco lost!'
                    yeetbot.state = yeetbot.state_return2aruco


            # move to strawberry
            elif yeetbot.state == yeetbot.state_move2strawberry:

                if strawberryDetector.exist == 1:
                    statusMessage = 'Moving towards the strawberry...'
                    distance2strawberry = sqrt((strawberryDetector.x)**2 + (strawberryDetector.z)**2)

                    # ensures that the yeetbot is neither too far away nor too close to the strawberry
                    if distance2strawberry > strawberryPickDistance:
                        yeetbot.followTarget((strawberryDetector.x)*2, moveSpeedStrawberry)

                    else:
                        yeetbot.backAwayFromTarget((strawberryDetector.x)*2, (moveSpeedStrawberry+5))
                    
                     # check if strawberry is in the center of the camera-view and within acceptable range
                    if abs(strawberryDetector.x) <= 35 and (strawberryPickDistance - 6) <= distance2strawberry <= (strawberryPickDistance+6):
                        statusMessage = 'Strawberry reached.'
                        yeetbot.stop()
                        yeetbot.state = yeetbot.state_pickStrawberry
                        
                else:
                    statusMessage = 'Strawberry lost!'
                    yeetbot.state = yeetbot.state_return2track


            # pick strawberry
            elif yeetbot.state == yeetbot.state_pickStrawberry: 
                
                # saves the detected y value of the strawberry as it will be obstructed from view when picking
                if pickStateMachine.state == pickStateMachine.saveStrawberryHeight and strawberryDetector.exist == 1:
                    savedStrawberryY = strawberryDetector.y
                    pickStateMachine.state = pickStateMachine.targetStrawberry


                # moves arm to the target strawberry
                elif pickStateMachine.state == pickStateMachine.targetStrawberry:
                    statusMessage = 'Targeting strawberry...'

                    if yeetbot.targetStrawberry(savedStrawberryY, servos) == True:
                        pickStateMachine.state = pickStateMachine.envelopStrawberry

                # moves forward towards the strawberry
                elif pickStateMachine.state == pickStateMachine.envelopStrawberry:
                    statusMessage = 'Extracting strawberry...'

                    if yeetbot.moveForward(1.3, 6) == True:
                        pickStateMachine.state = pickStateMachine.catchStrawberry

                # closes hand around the strawberry
                elif pickStateMachine.state == pickStateMachine.catchStrawberry:

                    if yeetbot.closeArm(servos) == True:
                        pickStateMachine.state = pickStateMachine.backAway

                # backs away from the strawberry
                elif pickStateMachine.state == pickStateMachine.backAway:

                    if yeetbot.moveBackward(1.1, 30) == True:
                        pickStateMachine.state = pickStateMachine.move2basket
                    
                # moves the strawberry to the basket
                elif pickStateMachine.state == pickStateMachine.move2basket:
                    statusMessage = 'Moving strawberry to basket...'

                    if yeetbot.move2basket(servos) == True:
                        pickStateMachine.state = pickStateMachine.release

                # releases the strawberry
                elif pickStateMachine.state == pickStateMachine.release:
                    
                    if yeetbot.openArm(servos) == True:
                        pickStateMachine.state = pickStateMachine.reset
                    

                # resets the arm
                elif pickStateMachine.state == pickStateMachine.reset:
                    statusMessage = 'Resetting arm...'

                    if yeetbot.resetArm(servos) == True:
                        statusMessage = 'Arm successfully reset.'
                        pickStateMachine.state = pickStateMachine.check
                
                # checks if the strawberry was picked successfully
                elif pickStateMachine.state == pickStateMachine.check:
                    
                    if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                        statusMessage = 'Strawberry picking failed or adjacent strawberry located.'
                        pickStateMachine.resetState()
                        yeetbot.state = yeetbot.state_move2strawberry

                    else:
                        statusMessage = 'Strawberry picked successfully!'
                        pickStateMachine.resetState()
                        yeetbot.state = yeetbot.state_return2aruco
                    
                    
            # return to track
            elif yeetbot.state == yeetbot.state_return2track:
                statusMessage = 'Returning to track...'

                if yeetbot.moveBackward(1, 15) == True:

                    if strawberryDetector.exist == 1 and distance2strawberry <= strawberryMinDistance:
                        yeetbot.state = yeetbot.state_move2strawberry

                    else:
                        yeetbot.state = yeetbot.state_return2aruco
                    

            # return to aruco
            elif yeetbot.state == yeetbot.state_return2aruco:
                statusMessage = 'Searching for previous ArUco...'

                if yeetbot.turnLeft(turnTime, turnSpeed) == True and aruco_id == aruco_id_previous[1]: # checks if the previous aruco is detected
                    statusMessage = 'Previous ArUco found!'
                    yeetbot.state = yeetbot.state_move2aruco 
            
            # stop yeetbot
            elif yeetbot.state == yeetbot.state_stop:
                statusMessage = 'Operation finished.'
                yeetbot.stop()
                    


            
            # Servo setpoints
            servoTargets.leftWheel  = yeetbot.leftWheel
            servoTargets.rightWheel = yeetbot.rightWheel

            # overwrites wheel servo values if joystick control is enabled
            if joystickControl == True:
                servoTargets.leftWheel  = leftWheelIdle - rightJoystick.y/2 + leftJoystick.x/7
                servoTargets.rightWheel = rightWheelIdle + rightJoystick.y/2 + leftJoystick.x/7
            

            servoTargets.servo1_target = yeetbot.servoTargets[0]
            servoTargets.servo2_target = yeetbot.servoTargets[1]
            servoTargets.servo3_target = yeetbot.servoTargets[2]
            
            # publishes servo setpoint values
            pub_servoTargets.publish(servoTargets)


            # Sleep remaining time
            rate.sleep()


    except rospy.ROSInterruptException:
        pass

