#!/usr/bin/env python
"""
Node for controlling Jetbot robot
"""
import rospy
import numpy as np
from std_msgs.msg import Int64
from mas507.msg import ServoSetpoints, ServoTargets
import Adafruit_PCA9685
import time
import math

# Yeetbot specific values
servo1_threshold = [200, 450]
servo2_threshold = [200, 500]
servo3_threshold = [200, 480]

servo2_range = [-20, 60] # [degrees] range of servo2

leftWheelIdle = 338
rightWheelIdle = 328

# User controllable variables
initialServos = [servo1_threshold[1], 450, servo1_threshold[0]]
servoRate = 100 #[Hz] Update rate of the servo controller
servoStep = 6 # Step value for incrementing the arm position of the robot


# Miscellaneous functions
def increment(value): # increments the first value of an array
    value[0] += 1
    return value[0]

def stepper(currentValue, stepSize, targetValue): # steps a value towards a target with zero overshoot or undershoot
    if currentValue < (targetValue - stepSize):
        currentValue += stepSize

    elif currentValue > (targetValue + stepSize):
        currentValue -= stepSize

    else:
        currentValue = targetValue

    return currentValue

def map(inputValue, inputMin, inputMax, outMin, outMax): # outputs a value in one range to a the equivalent in another range
        return outMin + ((outMax - outMin)/(inputMax - inputMin))*(inputValue - inputMin)


class Yeetbot():
    def __init__(self):
        self.leftWheel = leftWheelIdle
        self.rightWheel = rightWheelIdle
        self.servoTargets = initialServos[:]
        self.driveBias = 0 # -1 turn left, 1 turn right, 0 no bias
        self.drive = False #variable used for timed movement

        b = [0]

        # states
        self.state_stop = increment(b) # stop yeetbot
        self.state_findAruco = increment(b) # search for an aruco marker
        self.state_move2aruco = increment(b) # moves to closest aruco marker
        self.state_move2strawberry = increment(b) # moves to closest strawberry
        self.state_pickStrawberry = increment(b) # picks strawberry
        self.state_return2track = increment(b) # return to track after successfull strawberry picking
        self.state_return2aruco = increment(b) # return to previous aruco

        # initial state
        self.state = self.state_findAruco


    def moveForward(self, time2move, moveSpeed): # move forward for a certain amount of time, returns true when finished moving
        self.leftWheel = leftWheelIdle + moveSpeed
        self.rightWheel = rightWheelIdle - moveSpeed
        
        if self.drive == False:
            self.drive = True
            return False
        else:
            time.sleep(time2move)
            self.drive = False
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            return True
        
    def moveBackward(self, time2move, moveSpeed): #move backward for a certain amount of time, returns true when finished moving
        self.leftWheel = leftWheelIdle - moveSpeed
        self.rightWheel = rightWheelIdle + moveSpeed
        
        if self.drive == False:
            self.drive = True
            return False
        else:
            time.sleep(time2move)
            self.drive = False
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            return True
        
    def stop(self): 
        self.leftWheel = leftWheelIdle
        self.rightWheel = rightWheelIdle
        
    def turnLeft(self, time2move, moveSpeed): 
        time.sleep(time2move)

        if self.drive == False:
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            self.drive = True
            return False
        else:
            self.leftWheel = leftWheelIdle - moveSpeed -10
            self.rightWheel = rightWheelIdle - moveSpeed
            self.drive = False
            return True
        

    def turnRight(self, time2move, moveSpeed):
        time.sleep(time2move)
        
        if self.drive == False:
            self.leftWheel = leftWheelIdle
            self.rightWheel = rightWheelIdle
            self.drive = True
            return False
        else:
            self.leftWheel = leftWheelIdle + moveSpeed
            self.rightWheel = rightWheelIdle + moveSpeed + 10
            self.drive = False
            return True
        

    def followTarget(self, posOffset, moveSpeed): # follow a specified target
        self.driveBias = float(posOffset)/600
        
        if self.driveBias >= 0: # right turn bias
            min(self.driveBias, 1)
            self.leftWheel = leftWheelIdle + moveSpeed
            self.rightWheel = rightWheelIdle - moveSpeed*(1-self.driveBias)
        else:
            max(self.driveBias, -1) # left turn bias
            self.leftWheel = leftWheelIdle + moveSpeed*(1+self.driveBias)
            self.rightWheel = rightWheelIdle - moveSpeed

    def backAwayFromTarget(self, posOffset, moveSpeed): # follow a specified target
        self.driveBias = float(posOffset)/600

        if self.driveBias >= 0: # right turn bias
            min(self.driveBias, 1)
            self.leftWheel = leftWheelIdle - moveSpeed*(1-self.driveBias)
            self.rightWheel = rightWheelIdle + moveSpeed
        else:
            max(self.driveBias, -1) # left turn bias
            self.leftWheel = leftWheelIdle - moveSpeed
            self.rightWheel = rightWheelIdle + moveSpeed*(1+self.driveBias)


    def targetStrawberry(self, strawberryY, currentServos):
        yOffset = 90 # [mm] robot arm motor axel height above floor
        armLength = 150 # [mm] length of the arm
        
        # calculate strawberry height to servo motor value
        armRad = math.asin(max(min(float(strawberryY-yOffset)/armLength, 1), -1))
        armDeg = armRad*180/np.pi
        armDegAdjusted = max(min(armDeg, servo2_range[1]), servo2_range[0])  # [degrees] target angle limited to servo2 range
        print(armRad, armDeg, strawberryY, armDegAdjusted)

        self.servoTargets[1] = int(map(armDegAdjusted, servo2_range[0], servo2_range[1] ,servo2_threshold[0] ,servo2_threshold[1]))

         # moves arm to the center of the view
        self.servoTargets[0] = 405

        
        if currentServos == self.servoTargets:
            return True
        else:
            return False


    def openArm(self, currentServos):
        self.servoTargets[2] = servo3_threshold[0]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False
        

    def closeArm(self, currentServos):
        self.servoTargets[2] = servo3_threshold[1]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False


    def move2basket(self, currentServos): 
        self.servoTargets[0] = 205
        self.servoTargets[1] = 230

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False
        

    def resetArm(self, currentServos):
        self.servoTargets = initialServos[:]

        # check if servos have reached their target values
        if currentServos == self.servoTargets:
            return True
        else:
            return False
        


class Joystick(object):
    def __init__(self):
        self.x = 0
        self.y = 0

    def callback(self, msg):
        self.x = msg.x
        self.y = msg.y


class PickStateMachine(): # state machine used for strawberry picking
    def __init__(self):
        b = [0]

        # states
        self.saveStrawberryHeight = increment(b) # saves the strawberry y-position
        self.targetStrawberry = increment(b) # moves arm to the strawberry position
        self.envelopStrawberry = increment(b) # envelops the strawberry
        self.catchStrawberry = increment(b) # closes the hand around the strawberry
        self.backAway = increment(b) # backs away from the strawberry
        self.move2basket = increment(b) # moves the strawberry to the basket
        self.release = increment(b) # releases the strawberry into the basket
        self.reset = increment(b) # resets the arm
        self.check = increment(b) # checks if the strawberry was successfully picked

        # initial state
        self.state = self.saveStrawberryHeight

    def resetState(self):
        self.state = self.saveStrawberryHeight


class YeetbotController(object):
    def __init__(self):
        # Adafruit PCA board instance setup
        self.adafruit = Adafruit_PCA9685.PCA9685(busnum=1)
        self.adafruit.set_pwm_freq(50)
        
    def setPWM(self, u):
        """
        Set RC parameter for wheel motors driven by Adafruit
        PCA9685 with 12 bit resolution (2^12 = 4096) for each servo
        On 50 Hz rate (20 ms period), 409 ticks=2ms, 204ticks=1ms

        Parameters
        ----------
        u[i]:
            Uptime of RC signal in ticks
            Between [204 and 408]
            where, theoretically, 306 is stand-still, 204 is maximum reverse
            and 408 is maximum forward.

            i=0: right motor
            i=1: left motor
            i=2: servo 1
            i=3: servo 2
            i=4: servo 3
        """

        for i in range(0, 5):
            # Limit input to u in [204, 408]
            u[i] = max(204, u[i])

            if i < 2:
                u[i] = min(408, u[i])

            else:
                u[i] = min(600, u[i])
            
            # Set PWM
            self.adafruit.set_pwm(i, 0, u[i])


def servoCallback(msg): # read data from ServoSetpoints message
    global servos
    
    servos = [msg.servo1, msg.servo2, msg.servo3]

def servoTargetsCallback(msg): # read data from ServoTargets message
    global rightWheel
    global leftWheel
    global servoTargets
    
    rightWheel = msg.rightWheel
    leftWheel = msg.leftWheel
    servoTargets = [msg.servo1_target, msg.servo2_target, msg.servo3_target]



if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('yeetbotController', anonymous=True)

        # Jetbot controller
        yeetbotController = YeetbotController()

        # Initialize servo setpoint values
        servoSetpoints = ServoSetpoints()
        servos = initialServos[:]
        servoTargets = initialServos[:]
        rightWheel = rightWheelIdle
        leftWheel = leftWheelIdle


        # Publisher
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)

        # Subscriber
        rospy.Subscriber("servoTargets", ServoTargets, servoTargetsCallback)
        rospy.Subscriber("servoSetpoints", ServoSetpoints, servoCallback)
        
        # Start Synchronous ROS node execution
        rate = rospy.Rate(servoRate)

        #t = 0

        while not rospy.is_shutdown():
            
            # coerce servo setpoint targets to their respective threshold values
            servoTargets[0] = max(min(servoTargets[0], servo1_threshold[1]), servo1_threshold[0])
            servoTargets[1] = max(min(servoTargets[1], servo2_threshold[1]), servo2_threshold[0])
            servoTargets[2] = max(min(servoTargets[2], servo3_threshold[1]), servo3_threshold[0])

            servoSetpoints.servo1 = stepper(servos[0], servoStep, servoTargets[0])
            servoSetpoints.servo2 = stepper(servos[1], servoStep, servoTargets[1])
            
            servoSetpoints.servo3 = servoTargets[2]

            
            # publishes servo setpoint values
            pub_servoSetpoints.publish(servoSetpoints)

            
            # Set Servo PWMs
            yeetbotController.setPWM([rightWheel, leftWheel, servos[0], servos[1], servos[2]])

            #print(t)
            #t += float(1)/servoRate

            # Sleep remaining time
            rate.sleep()


    except rospy.ROSInterruptException:
        pass